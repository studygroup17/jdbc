package com.greedy.section01.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.greedy.section01.controller.OrderController;
import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;

// 전체 흐름: 사용자에게 메뉴테이블을 보여주고 원하는 메뉴별 주문 수량을 입력받아 누적시키고 
// 			주문이 끝나면 주문 총 가격과, 메뉴별 주문 총 수량을 구해 
//			1. ORDER 테이블에 INSERT하고 
//			시퀀스로 자동생성된 주문번호를 SELECT해 생성된 주문번호 조회
//			ORDER_MENU에 조회한 주문번호, 메뉴명, 주문수량을 INSERT한다.
public class OrderMenu {
	
	// 다음 계층과 연결
	private OrderController orderController = new OrderController();
	
	// application 에서 보여줄 화면 담당.
	public void displayMainMenu() {

		/* 누적 시킬 값을 담을 변수들 선언 */
		// 한 주문내 각 메뉴별(행)을 쌓을 List 선언.(반복문 한번이 한 행이 된다.) (OrderMenuDTO에 사용)
		List<OrderMenuDTO> orderMenuList = new ArrayList<>();
		
		// 한 주문의 총 누적 금액을 알기위한 변수 선언. (OrderDTO에 사용)
		int totalOrderPrice = 0;
		
		Scanner sc = new Scanner(System.in);
		
		// 사용자가 주문하고 추가 주문이 없다면("아니오" 입력) flag를 false로 바꿔 반복을 정지시킨다.
		boolean flag = true;
		do {
			System.out.println("=========== 음식 주문 프로그램 ===========");
			
			// DB의 전체 카테고리를 DAO에서 categoryList에 담아 계층 순서대로 view까지 호출
			List<CategoryDTO> categoryList = orderController.selectAllCategory();
			
			// 담은 전체 카테고리를 한 행씩 출력해 사용자에게 보여준다.
			for(CategoryDTO cate : categoryList) {
				System.out.println(cate);
			}
			
			System.out.println("=====================================");
			System.out.print("주문하실 카테고리 종류의 이름을 입력해 주세요: ");
			String inputCategory = sc.nextLine();
			
			System.out.println("=========== 주문 가능 메뉴 ============");
			
			/* 입력받은 값 가공 처리는 Controller에게 맡기고 가공 처리에 필요한 값을 넘긴다. */
			// 사용자에게 입력받은 inputCategory를 전달해 PSTMT의 placeholder에 대입하고 반환된 카테고리 다중행을 list에 담아 리턴.
			List<MenuDTO> menuList = orderController.selectMenuBy(inputCategory, categoryList);
			
			// 리턴받은 다중행을 한 행씩 출력한다.
			for(MenuDTO menu : menuList) {
				System.out.println(menu);
			}
			
			System.out.print("주문하실 메뉴를 선택해 주세요: ");
			String inputMenu = sc.nextLine();
			
			/* Controller에서 처리할 수 없는 가공처리는 화면단에서 처리한다.(누적값은 화면단에서 처리되야 되므로) */
			int menuCode = 0;
			int menuPrice = 0;
			
			// 사용자가 입력한 카테고리의 메뉴들을 보고 한 메뉴를 입력하면 각 행의 menu.getName()을 사용자 입력값(inputMenu)과 
			// .equals 해서 일치하는 행의 menuCode, menuPrice를 저장(후에 사용하기 위해 main으로 나와있다.)
			for(int i = 0; i < menuList.size(); i++) {
				MenuDTO menu = menuList.get(i);
				if(menu.getName().equals(inputMenu)) {
					menuCode = menu.getCode();
					menuPrice = menu.getPrice();
				}
			}
			
			System.out.print("주문하실 수량을 입력하세요: ");
			int orderAmount = sc.nextInt();
			
			// 사용자가 입력한 수량과 바로 직전에 입력한 메뉴의 가격을 곱하여 한 메뉴의 총 가격을 주문 총가격에 += 담는다.
			// (만약 다른 메뉴도 주문할 경우 총 가격에 더해야 하기 때문에 += 사용, do() while문(주문) 밖에 선언.)
			totalOrderPrice += menuPrice * orderAmount;
			
			// 사용자로부터 입력받은 메뉴의 코드와, 개수를 orderMenuDTO에 담아 List 한 행으로 쌓는다.
			// (만약 다른 메뉴도 주문할 경우 한 주문(order) 안에서 실행되기 때문)
			OrderMenuDTO orderMenu = new OrderMenuDTO();
			orderMenu.setMenuCode(menuCode);
			orderMenu.setAmount(orderAmount);
			
			orderMenuList.add(orderMenu);
			
			sc.nextLine();
			String isContinue = "";
			// 사용자에게 추가주문을 할 것인지 묻고 "예" 일경우 반복, "아니오" 일경우 flag를 false로 만들어 반복 정지.
			// (만약 사용자가 같은 메뉴로 추가 주문할 경우 오류 발생: DB의 ORDER 테이블의 주문코드와 메뉴코드는 복합키로 지정되어 있음(중복시 오류))
			while(true) {
				System.out.print("계속 주문하시겠습니까?(예/아니오): ");
				isContinue = sc.nextLine();
				if("예".equals(isContinue)) {
					break;
				} else if("아니오".equals(isContinue)) {
					flag = false;
					break;
				}
			}
			
		} while(flag);
		
		/* 전체 주문 관련 내용을 이번에는 Map(DTO 말고)에 담아 Controller로 전달 */
		// 주문이 끝난 후. MAP에 "주문 총 가격"과, 한 메뉴씩 시킨 수량이 담긴 "주문메뉴 리스트"를 담아 전달인자로 전달.
		// 재 수강후 알아햐 할 것 : 
		// 왜 map을 사용했는가?
		// answer: 웹상에서 백엔드로 넘어올때 map으로 넘어오기 때문에 사용. (String형태로 넘어온다)
		// 왜 Object를 사용했는가?
		// totalOrderPrice는 integer, orderMenuList는 List 형태로 서로 다른 자료형 이기때문에 둘다 상속 되어있는 Object 사용
		Map<String, Object> requestMap = new HashMap<>();
		requestMap.put("totalOrderPrice", totalOrderPrice);
		requestMap.put("orderMenuList", orderMenuList);
		
		// 사용자에게 입력받은 값을 바탕으로 완성된 주문 총 가격과, 한 주문내 메뉴별 주문 수량을 orderController.registOrder()에 전달
		// 트랜잭션 성공 여부는 controller에서 가공된 뷰를 보여준다.
		orderController.registOrder(requestMap);
	}
}








