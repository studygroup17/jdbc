package com.greedy.section01.model.dao;

import static com.greedy.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;

public class OrderDAO {
	// 다음 계층과 연결(DB)
	private Properties prop = new Properties();
	
	public OrderDAO() {
		try {
			// 객체 생성시 자동으로 쿼리 xml을 load 한다.
			prop.loadFromXML(new FileInputStream("mapper/order-query.xml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	// DB의 전체 카테고리를 DAO에서 categoryList에 담아 계층 순서대로 view까지 호출
	public List<CategoryDTO> selectAllCategory(Connection con) {
		// 사용자로부터 입력받지 않기 때문에 statement 사용
		Statement stmt = null;
		// select 조회값을 받아야 하기 때문에 ResultSet사용
		ResultSet rset = null;
		// ResultSet의 각 행(CategoryDTO)을 담기위한 List
		List<CategoryDTO> categoryList = new ArrayList<>(); 	// NPE 방지 코드
		
		// selectAllCategory키의 키값을 쿼리로 지정
		String query = prop.getProperty("selectAllCategory");
		
		try {
			// statement 생성
			stmt = con.createStatement();
			// statement에 쿼리를 넣어 DB에 전달하고 반환값 rset 대입
			rset = stmt.executeQuery(query);
			
			// 현재 rset에는 전체 카테고리 행들을 담고 있다.
			// 각 행들은 한번씩 뽑아내며 반복
			while(rset.next()) {
				// 각 행마다 다른 CategoryDTO에 담기 위해 안에 생성
				CategoryDTO category = new CategoryDTO();
				// 카테고리의 코드,카테고리명, 상위카테고리를 CategoryDTO에 담는다.
				category.setCode(rset.getInt("CATEGORY_CODE"));
				category.setName(rset.getString("CATEGORY_NAME"));
				category.setRefCode(rset.getInt("REF_CATEGORY_CODE"));
				
				// CategoryDTO에 담겨 완성된 행은 categoryList에 각각 담긴다.
				categoryList.add(category);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// 사용된 ResultSet Statement는 닫아 준다.
			close(rset);
			close(stmt);
		}
		
		// 전체 카테고리 행을 담은 List는 Service > Controller > view(OrderMenu)에 리턴되어 사용자에게 보여진다.
		return categoryList;
	}

	// 사용자가 카테고리들을 보고 입력한 카테고리의 메뉴들을 조회하는 메소드
	public List<MenuDTO> selectMenuBy(Connection con, int categoryCode) {
		// 사용자에게 입력받은 값을 사용하기 때문에 PreparedStatement 선언
		PreparedStatement pstmt = null;
		// SELECT문으로 조회값이 존재하기 때문에 받을 ResultSet 선언
		ResultSet rset = null;

		// 조회된 특정 카테고리의 각 행(메뉴)를 담기위한 List 선언.
		List<MenuDTO> menuList = new ArrayList<>();
		
		// 특정 카테고리를 조회하기 위한 쿼리인 selectMenuByCategory 키값으로 초기화
		String query = prop.getProperty("selectMenuByCategory");
		
		try {
			// PreparedStatement는 생성과 동시에 쿼리를 넣는다.
			pstmt = con.prepareStatement(query);
			// PreparedStatement안에 plaseholder(?)에 값을 초기화 한다.
			// 초기화 값은 사용자로부터 입력받은 카테고리 코드
			pstmt.setInt(1, categoryCode);
			
			// 완성된 쿼리문으로 DB조회해 ResultSet은 rset에 담는다.
			rset = pstmt.executeQuery();
			
			// rset에 담긴 특정 카테고리의 각 행(메뉴)을 한번씩 뽑으며 반복.
			while(rset.next()) {
				// 각 행 별로 생성한 MenuDTO에 담는다. (메뉴코드, 메뉴명, 메뉴 가격, 카테고리 코드, 판매 가능상태)
				MenuDTO menu = new MenuDTO();
				menu.setCode(rset.getInt("MENU_CODE"));
				menu.setName(rset.getString("MENU_NAME"));
				menu.setPrice(rset.getInt("MENU_PRICE"));
				menu.setCategoryCode(rset.getInt("CATEGORY_CODE"));
				menu.setOrderableStatus(rset.getString("ORDERABLE_STATUS"));
				
				// MenuDTO에 특정 컬럼들을 넣어 완성된 행은 menuList에 각각 쌓는다.
				menuList.add(menu);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// 사용된 ResultSet, PreparedStatement는 닫아준다.
			close(rset);
			close(pstmt);
		}
		
		// 특정 카테고리의 각 행들을 쌓은 menuList는 > DAO > Service > Controller > view(OrderMenu)까지 리턴되어 사용자에게 보여진다.
		return menuList;
	}
	// 사용자로부터 입력받은 주문을 바탕으로 주문 총 가격과, 주문 날짜 및 시간을 OrderDTO에 담아 생성된 Connection과 함게 매개변수로 전달 받아 INSERT
	public int registOrder(Connection con, OrderDTO order) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertOrder");
		
		try {
			pstmt = con.prepareStatement(query);
			// 주문 날짜를 ?에 초기화(ORDER_DATE위치)
			pstmt.setString(1, order.getDate());
			// 주문 시간을 ?에 초기화(ORDER_TIME위치)
			pstmt.setString(2, order.getTime());
			// 주문 총 가격을 ?에 초기화(TOTAL_ORDER_PRICE위치)
			pstmt.setInt(3, order.getTotalOrderPrice());
			
			// DB에 쿼리를 전달 및 실행해 조회값 유무인 int 반환값으로 result초기화
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			// 반환값까지 받은 후 PreparedStatement 닫기
			close(pstmt);
		}
		
		// 반환값 유무인 int값을 리턴한다. 이는 Service에서 가공되어 Controller에 view선택에 영향
		return result;
	}
	// registOrder로 Order를 insert하며 생성된 주문번호(시퀀스)를 조회하는 메소드.
	public int selectLastOrderCode(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		
		int lastOrderCode = 0;
		
		String query = prop.getProperty("selectLastOrderCode");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				lastOrderCode = rset.getInt("CURRVAL");
				// CURRVAL는 그냥 명칭을 사용해도 뽑혀 나온다.
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(stmt);
		}
		
		return lastOrderCode;
		
		
		
	}

	public int registOrderMenu(Connection con, OrderMenuDTO orderMenu) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		// 주문받은 각 메뉴를 insert하는 쿼리를 불러온다.
		// 한 주문에서 각 메뉴별로 insert한다.
		String query = prop.getProperty("insertOrderMenu");
		
		try {
			// insertOrderMenu 쿼리에 plaseholder 3부분을 채워 전달
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, orderMenu.getOrderCode());
			pstmt.setInt(2, orderMenu.getMenuCode());
			pstmt.setInt(3, orderMenu.getAmount());
			
			// 전달하고 반환값 result 대입
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// pstmt 닫기
			close(pstmt);
		}
		
		// 반환값을 Service로 반환.
		return result;
	}
}





