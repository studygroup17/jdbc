package com.greedy.section01.model.service;

import static com.greedy.common.JDBCTemplate.close;
import static com.greedy.common.JDBCTemplate.getConnection;
import static com.greedy.common.JDBCTemplate.commit;
import static com.greedy.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.List;

import com.greedy.section01.model.dao.OrderDAO;
import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;

public class OrderService {
	// 다음 계층과 연결
	private OrderDAO orderDAO = new OrderDAO();

	// DB의 전체 카테고리를 DAO에서 categoryList에 담아 계층 순서대로 view까지 호출
	public List<CategoryDTO> selectAllCategory() {
		// service 계층에서 connection을 생성한다.
		Connection con = getConnection();
		// 입력값 없이 사용자에게 보여줄 select문 이므로 orderDAO.selectAllCategory에 생성(연결)된 connection 만 보내준다.
		List<CategoryDTO> categoryList = orderDAO.selectAllCategory(con);
		// 반환값까지 받으면 connection을 닫아 준다.
		close(con);
		
		// orderDAO.selectAllCategory으로부터 반환받은 전체카테고리 조회값을 반환
		// > Controller > view(OrderMenu)를 통해 사용자에게 출력
		return categoryList;
	}
	
	// 사용자에게 입력받은 카테고리를 Controller에서 가공해 전달받아 생성한 Connection과 함께 DAO로 전달하는 메소드
	public List<MenuDTO> selectMenuBy(int categoryCode) {
		Connection con = getConnection();
		
		// 받은 매개변수를 생성(연결)된 Connection과 함께 orderDAO.selectMenuBy()에 전달.
		List<MenuDTO> menuList = orderDAO.selectMenuBy(con, categoryCode);
		
		// 반환값까지 받으면 Connection을 닫아 준다.
		close(con);
		
		// orderDAO.selectMenuBy()로 부터 반환받은 사용자가 입력한 카테고리명과 일치하는 메뉴들의 조회값을 반환.
		// > Controller > view(OrderMenu)를 통해 사용자에게 출력
		return menuList;
	}
	
	// 사용자에게 전부 입력받은 주문들 바탕으로 주문 날짜, 시간, 주문 총 가격과, 메뉴별 주문 수량(List)를 
	// DTO에 담아 매개변수로 받아 DAO에 Connection과 함께 전달.
	public int registOrder(OrderDTO order) {
		
		/* 1. Connection 생성 */
		Connection con = getConnection();
		
		/* 2. 트랜잭션 결과(최종결과)를 담을 하나의 변수 */
		int result = 0;
		
		/* 3-1. ORDER TABLE INSERT */
		// 주문 날짜, 시간, 주문 총 금액만 INSERT (주문 번호는 시퀀스로 자동생성된다.)
		int orderResult = orderDAO.registOrder(con, order);
		
		/* 3-2. 마지막 주문 시퀀스 SELECT */
		// 직전에 INSERT하며 자동 생성된 주문번호 코드를 알기위해 select
		int orderCode = orderDAO.selectLastOrderCode(con);
		
		/* 3-3. ORDER_MENU TABLE INSERT */
		/* 주문 번호를 이제 알았으니 채우는 작업을 하자 */
		// orderDTO에 넣어뒀던 orderMenuList를 꺼낸다.
		List<OrderMenuDTO> orderMenuList = order.getOrderMenuList();
		
		// orderMenuList에서 각 행을 꺼내 orderCode를 입력.
		// orderMenuDTO에서 채우지 못한 주문번호를 넣어 각 행별 주문번호, 메뉴코드, 주문 수량을 완성한다.
		// 왜 그걸 Service에서 하지?
		// 한 트랜잭션 내에서 끝낼 수 있기 때문이다.
		for(int i = 0; i < orderMenuList.size(); i++) {
			// 행을 orderMenuDTO에 담고
			OrderMenuDTO orderMenu = orderMenuList.get(i);
			// orderMenuDTO에 비워진 orderCode값을 채운다.
			orderMenu.setOrderCode(orderCode);
			// orderMenu에 담기는건 한 행만 되는거 아닌가?
			// answer: 어차피 한 주문내에서 주문코드를 넣는 작업이여서 무관. 
			// orderMenuList에는 다시 값이 적용되나?
		}
		
		// 각 주문한 메뉴별로 INSERT가 잘 되었는지 확인하기 위한 축적 변수 선언
		int orderMenuResult = 0;
		for(int i = 0; i < orderMenuList.size(); i++) {
			// orderMenuList행을 orderMenu에 담고
			OrderMenuDTO orderMenu = orderMenuList.get(i);
			
			// 각 행을  DAO로 보내고 값을 orderMenuResult에 축적
			// 각 행을 보내는 이유: 한 주문내에서 한 메뉴당 주문 수량을 각 행으로 INSERT 하기 때문.
			orderMenuResult += orderDAO.registOrderMenu(con, orderMenu);
		}
		
		/* 4. 트랜잭션 처리 */
		// orderResult 조회값이 있으면서 orderMenuResult값이 주문한 메뉴만큼 정상적으로 INSERT되어 반환값이 축적 되었을 때 
		if(orderResult > 0 && orderMenuResult == orderMenuList.size()) {
			// commit하며 Controller에 1을 반환
			commit(con);
			result = 1;
		} else {
			// 하나라도 정상적으로 완료되지 않았다면 rollback 한다.
			rollback(con);
		}
		
		/* 5. Connection 닫기 */
		close(con);
		
		/* 6. 최종 결과 값 반환 */
		return result;
	}
}








