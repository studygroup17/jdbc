package com.greedy.section01.controller;

import java.text.SimpleDateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;
import com.greedy.section01.model.service.OrderService;
import com.greedy.section01.view.ResultView;

/*
 * Controller Layer(계층)의 역할
 * 1. 사용자가 입력한 정보를 파라미터 형태로 전달받아 전달받은 값을 검증하거나 가공
 * 2. Service의 메소드 호출
 * 3. 수행 결과를 반환 받아 사용자에게 보여줄 뷰 결정
 * 4. 뷰에 필요한 데이터를 전달
 */
public class OrderController {
	// 다음 계층과 연결
	private OrderService orderService = new OrderService();

	// DB의 전체 카테고리를 DAO에서 categoryList에 담아 계층 순서대로 view까지 호출
	public List<CategoryDTO> selectAllCategory() {
		// 가공하거나, 보여줄 컬럼들이 이미 결정되어 있기때문에, 바로 Service메소드를 호출해 반환값 리턴.
		return orderService.selectAllCategory();
	}

	// 사용자에게 view에서 입력받은 값을 조회한 전체카테고리를 사용해 가공  처리.
	public List<MenuDTO> selectMenuBy(String inputCategory, List<CategoryDTO> categoryList) {
		// 사용자에게 입력받은 값과 일치하는 카테고리의 코드로 초기화할 변수
		int categoryCode = 0;
		// 반복문 으로 각 카테고리마다 사용자 입력값(카테고리명)과 대조해 일치하는 값을 categoryCode변수에 초기화.
		for(int i = 0; i < categoryList.size(); i++) {
			CategoryDTO category = categoryList.get(i);
			if(category.getName().equals(inputCategory)) {
				categoryCode = category.getCode();
				// 일치하면 다음 행들은 실행하지 않도록 break해 효율을 높인다.
				break;
			}
		}
		
		// 사용자 입력값을 가공해 orderService.selectMenuBy 메소드에 전달하고 반환값을 view에 리턴
		return orderService.selectMenuBy(categoryCode);
	}
	
	// 사용자에게 입력받은 값들을 바탕으로 Order 테이블에 insert한다. Map<"키", 모든 자료형>
	// 사용자에게 입력받은 값들을 가공. Map으로 전달받은 키의 값을 꺼내 변수와 OrderMenuDTO에 담았다.
	public void registOrder(Map<String, Object> requestMap) {
		
		/* 1. 뷰에서 전달 받은 파라미터를 꺼내서 각각의 변수에 담기 */
		// 재수강후 알아야 할 것 :
		// 왜 (integer)을 사용했는가?
		// totalOrderPrice의 키 값은 int형 이고 map에서 Object로 형변환이 되어있기때문에 다시 강제형변환 .
		// 전달받은 Map에서 총 주문 가격에 대한 키값을 변수에 초기화
		int totalOrderPrice = (Integer)requestMap.get("totalOrderPrice");
		// 전달받은 Map에서 한 주문 내 각 메뉴별 주문수량에 대한 키값을 꺼내 orderMenuList에 각각 초기화.
		List<OrderMenuDTO> orderMenuList = (List<OrderMenuDTO>)requestMap.get("orderMenuList");
		
		/* 2. 화면단에서 미처 제공하지 못했거나 가공처리해야 할 것들을 가공처리 하자 */
		// order 테이블을 미처 제공하지 못한 이유: order테이블에 사용되야할 주문 총 가격은 사용자 입력값을 받고 마지막에 나오기 때문.
		/* 2-1. 주문 날짜와 시간 구하기 */
		java.util.Date orderTime = new java.util.Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yy/MM/dd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		String date = dateFormat.format(orderTime);
		String time = timeFormat.format(orderTime);
		
		/* 2-2. 서비스 쪽으로 전달하기 위해 DTO 인스턴스로 만들기(묶기) */
		// 왜 order테이블 컬럼에 없는 orderMenuList를 넣지?
		// orderDTO안의 값들을 사용하면 order테이블에 INSERT할 수 있다. 
		// 그렇게 되면 자동으로 orderMenuList에 필요한 주문번호를 알 수 있고 이를통해 orderMenu 또한 INSERT가능하기 때문.
		OrderDTO order = new OrderDTO();
		order.setDate(date);
		order.setTime(time);
		order.setTotalOrderPrice(totalOrderPrice);
		order.setOrderMenuList(orderMenuList);
		
		/* 3. Service 메소드를 호출하고 결과를 리턴받음 */
		// DB에 주문의 날짜 , 시간, 총 금액을 insert하면 시퀀스(주문번호) 자동 생성된다.
		int result = orderService.registOrder(order);
		
		/* 4. Service 처리 결과를 이용해서 성공 실패 여부를 판단하여 사용자에게 보여줄 뷰를 결정하고 뷰에 필요한 값을 전달함 */
		// Service로 부터 전달받은 트랜잭션 성공 및 실패 여부를 가공해 사용자에게 뷰를 보여준다.
		ResultView resultView = new ResultView();
		if(result > 0) {
			resultView.success(orderMenuList.size());
		} else {
			resultView.failed();
		}
		// 처리 결과 성공 실패 시 페이지를 보여준다.
	}
	
	
	
}


