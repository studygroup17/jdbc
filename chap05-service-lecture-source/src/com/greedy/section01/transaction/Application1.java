package com.greedy.section01.transaction;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import static com.greedy.common.JDBCTemplate.getConnection;
import static com.greedy.common.JDBCTemplate.close;

public class Application1 {

	/*
	 * 1. autoCommit 상태가 true(default)일 때 fianlly를 통한 Connection을 close 했을 때
	 * -> Connection을 close 하면서 commit 작업이 내부적으로 발생한다.
	 * 
	 * 2. autoCommit 상태를 false로 두고 finally를 통한 Connection을 close했을 때
	 * -> Connection을 close 하면서 commit 작업이 내부적으로 발생한다.
	 * (autocommit 상태가 true 라는 건 하나의 트랜잭션 안에서 두 개 이상의 DML작업이 있을 때 하나의 DML작업당
	 *  commit을 자동으로 한다는 것이고, autoCommit 상태가 false라는 건 하나의 트랜잭션 안에서 두 개 이상의 DML작업이
	 *  끝나면 개발자가 수동으로  commit을 한다는 의미이다. )
	 */
	public static void main(String[] args) {
		Connection con = getConnection();
		try {
			System.out.println("autoCommit의 현재 설정값: " + con.getAutoCommit());
			// insert/update 등 한번 실행 시키면 그 즉시 commit 시킨다.
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		Properties prop = new Properties();
		
		try {
			
			prop.loadFromXML(new FileInputStream("mapper/menu-query.xml"));
			
			String query = prop.getProperty("insertMenu");
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, "참치마요");
			pstmt.setInt(2, 25000);
			pstmt.setInt(3, 4);
			pstmt.setString(4, "Y");
			
			result = pstmt.executeUpdate();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(con);			// Connection을 close 할 때 commit
		}
		
		if(result > 0) {
			System.out.println("메뉴 등록 성공");
		} else {
			System.out.println("메뉴 등록 실패");
		}
	}

}
