package com.greedy.section02.model.dto;

import java.io.Serializable;

public class CategoryDTO implements Serializable{
	private static final long serialVersionUID = -1319772209459856409L;
	
//	CATEGORY_CODE	NUMBER
//	CATEGORY_NAME	VARCHAR2(30 BYTE)
//	REF_CATEGORY_CODE	NUMBER
//	number을 변환할 때 데이터를 확인하고 소수점이 있는지 확인
	
	private int code;			// 카테고리 코드
	private String name;		// 카테고리명
	private Integer refCode;	// 상위카테고리코드(null이 들어올 수 도 있으므로 Wrapper클래스 타입으로 선언한다)
	//int는 기본자료형이고 integer은 참조 자료형이니 null이 가능하다.
	
	public CategoryDTO() {
	}
	
	public CategoryDTO(int code, String name, Integer refCode) {
		this.code = code;
		this.name = name;
		this.refCode = refCode;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getRefCode() {
		return refCode;
	}

	public void setRefCode(Integer refCode) {
		this.refCode = refCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "CategoryDTO [code=" + code + ", name=" + name + ", refCode=" + refCode + "]";
	}
	
	
	
	
	

}
