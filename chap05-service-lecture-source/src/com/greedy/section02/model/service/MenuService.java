
package com.greedy.section02.model.service;

import static com.greedy.common.JDBCTemplate.close;
import static com.greedy.common.JDBCTemplate.getConnection;
import static com.greedy.common.JDBCTemplate.commit;
import static com.greedy.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.greedy.section02.model.dao.MenuDAO;
import com.greedy.section02.model.dto.CategoryDTO;

/*
 * Service Layer(계층)의 역할
 * 1. Connection 생성
 * 2. DAO의 메소드 호출(Connection과 입력값 전달)
 * 3. 트랜잭션 제어
 * 4. Connection 닫기(반납)
 */
// 구분하는 이유. 유지보수, 생산성, 가독성  : (오류가 발생했을때 해당 layer만 보면 된다.)

// Service 메소드는 트랜잭션 단위로 나눈다. 즉, Connection도 트랜잭션 단위마다 열고 닫는다.
public class MenuService {

	/* 신규 메뉴 등록용 서비스 메소드 */
	public void registMenu() {					// Service 계층의 메소드 단위는 트랜잭션 단위이다.
			
		/* 1. Connection 생성 */
		Connection con = getConnection();
		
		/* 2. DAO의 메소드 호출 */
		MenuDAO menuDAO = new MenuDAO();		// 쿼리를 불러오는 작업을 MenuDAO 기본생성자에 생성
		
		/* 2-1. 카테고리 등록 */
		CategoryDTO newCategory = new CategoryDTO();
		newCategory.setName("에피타이저");
		newCategory.setRefCode(null);			// DTO를 가서 refCode 속성의 자료형을 Integer로 지정해야 한다.
		// 모델링 과정에서 컬럼제약조건이 nullable인 것을은 wrapper 클래스를 미리 지정하면 된다.
		
		int result1 = menuDAO.insertNewCategory(con, newCategory);
		System.out.println("카테고리 추가 여부: " + result1);
		
		/* 2-2. 방금 추가한 마지막 카테고리의 번호 조회 */
		int lastCategoryCode = menuDAO.selectLastCategoryCode(con);
		System.out.println("마지막 카테고리 번호: " + lastCategoryCode);
		
		/* 2-3. 신규 메뉴 등록 */
		Map<String, String> newMenu = new HashMap<>();
		newMenu.put("name", "콩자반김치국");
		newMenu.put("price", "5000");
		newMenu.put("category", Integer.valueOf(lastCategoryCode).toString()); // int기본자료형을 wrapper클래스로 바꿔 String 형태로 바꾼다.
		newMenu.put("orderable", "Y");
		
		int result2 = menuDAO.insertNewMenu(con, newMenu);
		
		/* 3. 트랜잭션 제어 */
		if(result1 > 0 && result2 > 0) {
			System.out.println("신규 카테고리와 메뉴를 모두 추가 성공!");
			commit(con);
		} else {
			System.out.println("신규 카테고리와 메뉴를 모두 추가 실패");
			rollback(con);
		}
		
		
		try {
			System.out.println("AutoCommit유뮤 : " + con.getAutoCommit());
			// 왜 fales가 나오는 것이지? 어디서 지정해 줬나?
			// greedy.common.JDBCTemplate에서 지정해줬고 해당 클래스를 하고 있기 때문.
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		/* 4. Connection 닫기 */
		close(con);
	}

}
