package com.greedy.section02.model.dao;

import static com.greedy.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Properties;

import com.greedy.section02.model.dto.CategoryDTO;

/*
 * DAO(Database Access Object): 데이터베이스 접근용 객체
 * => CRUD를 담당하는 메소드들의 집합으로 이뤄진 클래스이다.
 * 
 * DAO의 역할
 * 1. Statement, PreparedStatement 생성
 * 2. 쿼리와 Statement 객체를 통한 CRUD
 * 3. ResultSet 또는 int값을 통한 DBMS로부터 응답 받기
 * 4. Statement 객체 및 ResultSet 닫기
 */

// CallableStatement는 PL/SQL문 사용
// 굳이 사용하지 않는다. 자바를 사용하면 되기 때문에
// 하지만, 사용하는 회사도 있다. 자바에서 따로 뺴서 유지보수성을 높이기 위해

// DAO 메소드는 쿼리 단위로 나눈다.
public class MenuDAO {
	private Properties prop = new Properties();
	
	public MenuDAO() {
		try {
			prop.loadFromXML(new FileInputStream("mapper/menu-query.xml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int insertNewCategory(Connection con, CategoryDTO newCategory) {		// DAO 계층의 메소드 단위는 실행 할 쿼리 단위이다.
		PreparedStatement pstmt = null;
		int result = 0;
		String query = prop.getProperty("insertCategory");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, newCategory.getName());
			pstmt.setObject(2, newCategory.getRefCode());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		return result;
	}

	public int selectLastCategoryCode(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		
		int lastCategoryCode = 0;
		
		String query = prop.getProperty("selectCurrentCategorySequenceValue");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				lastCategoryCode = rset.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(stmt);
		}
		
		return lastCategoryCode;
	}

	public int insertNewMenu(Connection con, Map<String, String> newMenu) {
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertMenu");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, newMenu.get("name"));
			pstmt.setString(2, newMenu.get("price"));
			pstmt.setString(3, newMenu.get("category"));
			pstmt.setString(4, newMenu.get("orderable"));
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		
		return result;
	}
}
