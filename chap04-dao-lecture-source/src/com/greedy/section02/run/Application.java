package com.greedy.section02.run;

import static com.greedy.common.JDBCTemplate.getConnection;
import static com.greedy.common.JDBCTemplate.close;

import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.greedy.model.dto.MenuDTO;
import com.greedy.section02.model.dao.MenuDAO;

public class Application {

	/*
	 * 사용자로부터 입력 받기 + Connection 인스턴스 생성하고 (첫 번째 계증(layer))
	 * Connection 인스턴스와 MenuDTO를 MenuDAO에 넘기는 역할
	 * 넘겨 받은 입력받은 값과 Connection을 활용해서 쿼리를 실행하고 결과를 반환 (두 번째 계층(layer))
	 */
	public static void main(String[] args) {
		Connection con = getConnection();
		
		MenuDAO registDAO = new MenuDAO();	// 쿼리도 처음부터 준비하게 한다.
		// 메뉴 추가용
		
		/* 1. 메뉴의 마지막 번호 조회 */
		int maxMenuCode = registDAO.selectLastMenuCode(con);
		
		System.out.println("기능을 호출한 곳에서 메뉴의 마지막 번호 반환값 확인: " + maxMenuCode);
		
		/* 2. 카테고리 조회 */
		/* 이번에는 CategoryDTO 대신에 Map으로 카테고리 정보를 한 행씩 받아내자 */
		List<Map<Integer, String>> categoryList = registDAO.selectAllCategory(con);
		
		for(Map<Integer, String> cate : categoryList) {
			System.out.println("cate" + cate);
		}
		
		/* 3. 신규 메뉴 등록 */
		Scanner sc = new Scanner(System.in);
		
		System.out.println("등록 할 메뉴의 이름을 입력하세요: ");
		String menuName = sc.nextLine();
		System.out.println("신규 메뉴의 가격을 입력하세요: ");
		int menuPrice = sc.nextInt();
		System.out.println("카테고리를 선택해 주세요(한식, 중식, 일식, 퓨전, 커피): ");
		sc.nextLine();
		String categoryName = sc.nextLine();
		System.out.println("바로 판매 메뉴에 적용하시겟습니까? (예/ 아니오): ");
		String answer = sc.nextLine();
		
		/* 사용자가 입력한 값을 가공(쿼리에 활용할 수 있는 값으로) */
		int categoryCode = 0;
		switch(categoryName) {
			case "한식": categoryCode = 4; break;
			case "중식": categoryCode = 5; break;
			case "일식": categoryCode = 6; break;
			case "퓨전": categoryCode = 7; break;
			case "커피": categoryCode = 8;
		}
		
		String orderableStatus = "";
		switch(answer) {
			case "예": orderableStatus = "Y"; break;
			case "아니오": orderableStatus = "N";
		}
		
		/* 입력받은 값을 하나로 만드는 방법(DTO로 만들 때) */
		/* 1. 매개변수 있는 생성자 활용 */
//		MenuDTO newMenu = new MenuDTO(maxMenuCode + 1, menuName, menuPrice, categoryCode, orderableStatus);
		// maxMenuCode + 1. 주의. +1안하면 중복 결과값 발생
		// 무결성 제약조건 위반 발생
		
		/* 2. setter를 활용 */
		MenuDTO newMenu = new MenuDTO();
		newMenu.setCode(maxMenuCode);
		newMenu.setName(menuName);
		newMenu.setPrice(menuPrice);
		newMenu.setCatergoryCode(categoryCode);
		newMenu.setOrderableStatus(orderableStatus); 
		
		
		int result = registDAO.insertNewMenu(con, newMenu);
		// 사용자가 입력해야 하는 쿼리를 사용해야 하니 newMenu도 넘겨주는 것. 완전할 경우 con만 넘기면 된다.
		// 왜냐하면 newMenu안의 들어있는 값으로 prepareStatement로 쿼리를 완성시켜야 하기 때문.
		if(result > 0) {
			System.out.println("메뉴 등록 성공!");
		} else {
			System.out.println("메뉴 등록 실패");
		}
		
		
		close(con);	// Connection 인스턴스는 사용하면 반납한다.
		// 만약 connnection을 닫지 않아도 메인이 끝나면 자동으로 commit된다(setAutoCommin(true);)
		// close를 닫아줘야 하는이유: 멀티 스레드 , 즉 사른사람들도동시에 사용하기 때문에 최신화 시켜야 함. 안하면 충돌난다.
	}
/*
 * dao : data accese object
데이터 베이스와 연결 및 연동 객체


** sql 문에서 max함수는 number, char, date 전부 사용가능하다. *
- number는 최대 값, char는 ㄱ~ ㅎ 시 ㅎ값, 또는 영어시 z값, date는 최신날짜

 */
}
