package com.greedy.section01.problem;

import static com.greedy.common.JDBCTemplate.close;
import static com.greedy.common.JDBCTemplate.getConnection;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import com.greedy.model.dto.CategoryDTO;


public class Application {

	/*
	 * 신규 메뉴를 등록하는 하나의 기능(Transaction)을 만드는데 필요한 절차
	 * 1. 메뉴의 마지막 번호 조회(SELECT) / 특정 PK는 시퀀스를 사용할 수 없다. 복합키(pk)일 경우 (중복 문제, cycle max 다름)
	 * 2. 카테고리 조회(SELECT)
	 * 3. 신규 메뉴 등록(INSERT)
	 * 3-1. 신규 메뉴 등록을 위한 정보 입력
	 * 3-2. 신규 메뉴 등록을 위한 값 제공
	 * 4. 정상 흐름과 예외 흐름에 대한 처리
	 */
	
//	기능 하나당 한번의 트랜잭션 단위 한 CONNECTION을 만들어 기능들을 주고 받는다.
	public static void main(String[] args) {
//		1. 준비 절차
		Connection con = getConnection();
		
		Statement stmt1 = null;
		Statement stmt2 = null;
		PreparedStatement pstmt = null;
		
		ResultSet rset1 = null;
		ResultSet rset2 = null;
		int result = 0;
		
		Properties prop = new Properties();
		
		try {
//			2. 마지막 메뉴 코드 조회 쿼리 읽어오기 및 query1변수에 담기
			prop.loadFromXML(new FileInputStream("mapper/menu-query.xml"));
			String query1 = prop.getProperty("selectLastMenuCode");
			String query2 = prop.getProperty("selectCategorylist");
			String query3 = prop.getProperty("insertMenu");
			
//			3. statement 생성 쿼리 전달
			stmt1 = con.createStatement();
			stmt2 = con.createStatement();
			pstmt = con.prepareStatement(query3);
			
//			<(1).메뉴의 마지막 번호 조회(SELECT)>
			rset1 = stmt1.executeQuery(query1);

			
//			4. 반환값 출력
			int maxMenuCode = 0;
			if(rset1.next()) {
				maxMenuCode = rset1.getInt(1);	// ResultSet 기준 끄집어 내는 방법 > 컬럼명, 별칭, 순서(연산처리한 컬럼은 별칭 또는 순번만 가능)
			}
			
//			System.out.println("마지막 메뉴 번호: " + maxMenuCode);
			
//			<(2).카테고리 조회(SELECT)>
			rset2 = stmt2.executeQuery(query2);
			
			List<CategoryDTO> categoryList = new ArrayList<>();
			
			while(rset2.next()) {
				CategoryDTO row = new CategoryDTO();
				
				row.setCode(rset2.getInt("CATEGORY_CODE"));
				row.setName(rset2.getString("CATEGORY_NAME"));
				
				categoryList.add(row);
			}
			
//			System.out.println("카테고리가 리스트에 잘 쌓였는지: " + categoryList);
//			System.out.println("카테고리가 리스트에 잘 쌓였는지: ");
//			for(CategoryDTO row : categoryList) {
//				System.out.println(row);
//			}
			
//			<(3).신규 메뉴 등록(INSERT) >
//			<(3-1).신규 메뉴 등록을 위한 정보 입력 >
			Scanner sc = new Scanner(System.in);
			System.out.println("등록 할 메뉴의 이름을 입력하세요: ");
			String menuName = sc.nextLine();
			System.out.println("신규 메뉴의 가격을 입력하세요: ");
			int menuPrice = sc.nextInt();
			
//			<(3-2).신규 메뉴 등록을 위한 값(카테고리 이름) 제공(전체 카테고리를 조회한 이유이기도 함, 지금은 생략)
			System.out.println("카테고리를 선택해 주세요(한식, 중식, 일식, 퓨전, 커피): ");
			sc.nextLine();
			String categoryname = sc.nextLine();
			System.out.println("바로 판매 메뉴에 적용하시겠습니까? (예/아니오): ");
			String answer = sc.nextLine();
			
			/* 사용자가 입력한 값을 가공 */
			int CategoryCode = 0;			// 쿼리에 적용될 값으로 가공처리한 값이 담길 변수
			switch(categoryname) {
				case "한식": CategoryCode = 4; break;
				case "중식": CategoryCode = 5; break;
				case "일식": CategoryCode = 6; break;
				case "퓨전": CategoryCode = 7; break;
				case "커피": CategoryCode = 8; break;
			}
			
			
			String orderableStatus = "";	// 쿼리에 적용될 값으로 가공처리한 값이 담길 변수
			switch(answer) {
				case "예": orderableStatus = "Y"; break;
				case "아니오": orderableStatus = "N"; break;
			}
			
			
			pstmt.setInt(1, maxMenuCode + 1);
			pstmt.setString(2, menuName);
			pstmt.setInt(3, menuPrice);
			pstmt.setInt(4, CategoryCode);
			pstmt.setString(5, orderableStatus);
			
			result = pstmt.executeUpdate();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset1);
			close(rset2);
			close(stmt1);
			close(stmt2);
			close(pstmt);
			close(con);
		}
		
		/* 4. 정상 흐름과 예외 흐름에 대한 처리 */
		if(result > 0) {
			System.out.println("메뉴 등록 성공");
		} else {
			System.out.println("메뉴 등록 실패");
		}
	}

}
