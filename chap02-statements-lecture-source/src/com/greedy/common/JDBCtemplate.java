package com.greedy.common;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

// 모든 패키지에서 공통적으로 쓰일 패키지

public class JDBCtemplate {

	public static Connection getConnection() {
		
		Connection con = null;
		
		Properties prop = new Properties();
		
		try {
			prop.load(new FileReader("config/connection-info.properties"));
			
//			System.out.println(prop);
			String driver = prop.getProperty("driver");
			String url = prop.getProperty("url");
			
			Class.forName(driver);
			
			con = DriverManager.getConnection(url, prop); // 확인용
			// con = DriverManager.getConnection(url,user, password); 가 기본 방식이지만.
			// (url, prop)으로 사용하고 싶으면prop안에 키 값이 user, password로 지정되어 있어야 된다. 
			// 그러면 getConnection이 알아서 꺼내 사용
			 
//			System.out.println("getConnection에서의 con: " + con); // 확인용
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return con;
	} // 커넷션 객체를 열기위해 반환하는 메소드.(닫으면 안된다.)

	/* Connection 인스턴스를 닫아주는 메소드(예외처리를 대신 하는 용도) */
	public static void close(Connection con) {
		try {
			if(con != null && !con.isClosed()) {	// 넘어 온 Connection 인스턴스가 존재하고 닫혀있지 않을 때
			// isClosed() : 닫혀 있다면 true를 반환.
			// Connection 인스턴스 자체가 스트림 이라 할 수 있다.
			con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void close(Statement stmt) {
		try {
			if(stmt != null && !stmt.isClosed()) {	// 넘어 온 Connection 인스턴스가 존재하고 닫혀있지 않을 때
			// isClosed() : 닫혀 있다면 true를 반환.
			// Connection 인스턴스 자체가 스트림 이라 할 수 있다.
			stmt.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void close(ResultSet rset) {
		try {
			if(rset != null && !rset.isClosed()) {	// 넘어 온 Connection 인스턴스가 존재하고 닫혀있지 않을 때
			// isClosed() : 닫혀 있다면 true를 반환.
			// Connection 인스턴스 자체가 스트림 이라 할 수 있다.
			rset.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}