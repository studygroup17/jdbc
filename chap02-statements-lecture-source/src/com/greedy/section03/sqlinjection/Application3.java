package com.greedy.section03.sqlinjection;

import static com.greedy.common.JDBCtemplate.getConnection;
import static com.greedy.common.JDBCtemplate.close;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.print.attribute.SetOfIntegerSyntax;

public class Application3 {

	private static String empId = "203";
	private static String empName = "' OR 1=1 AND EMP_ID = '203";
	
	public static void main(String[] args) {
		Connection con = getConnection();	
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = "SELECT * FROM EMPLOYEE WHERE EMP_ID = ? AND EMP_NAME = ?";
		// ?에 싱글쿼테이션을 못 쓰게 막아놓았기 때문에 불가능
		// ?에 싱글쿼테이션도 이름으로 받아 들인다.
		System.out.println(query);
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, empId);
			pstmt.setString(2, empName);
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				System.out.println(rset.getString("EMP_NAME") + "님 환영합니다. ");
			} else {
				System.out.println("회원 정보가 없습니다.");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
			close(con);
		}
		
		
		
		
	}

}
