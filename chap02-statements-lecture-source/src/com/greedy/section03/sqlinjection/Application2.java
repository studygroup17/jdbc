package com.greedy.section03.sqlinjection;

import static com.greedy.common.JDBCtemplate.getConnection;
import static com.greedy.common.JDBCtemplate.close;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Application2 {

	private static String empId = "203";
	private static String empName = "' OR 1=1 AND EMP_ID = '203";
	
	public static void main(String[] args) {
		Connection con = getConnection();
		Statement stmt = null;
		ResultSet rset = null;
		
		String query = "SELECT * FROM EMPLOYEE WHERE EMP_ID = '" + empId + "' AND EMP_NAME = '" + empName + "'";
		// AND 먼저 계산, 오른쪽은 항상 TRUE 발생, OR은 둘 중 하나만 TRUE여도 TRUE 이므로 PASS된다.
		System.out.println(query);
		// SELECT * FROM EMPLOYEE WHERE EMP_ID = '203' AND EMP_NAME = '' OR 1=1 AND EMP_ID = '203'
		
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				System.out.println(rset.getString("EMP_NAME") + "님 환영합니다. ");
			} else {
				System.out.println("해당하는 회원 정보가 없습니다.");
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(stmt);
			close(con);
		}
		
		
		
	}

}
