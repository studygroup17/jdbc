package com.greedy.section02.preparedStatement;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/* 쿼리를 저장 할 xml 파일 생성용 예제 */
public class test {

	public static void main(String[] args) {
		
		Properties prop = new Properties();
		prop.setProperty("keyString", "valueString");
		// 샘플데이터를 만들어 놔야 태그들이 만들어 진다.
		
		try {
			prop.storeToXML( new FileOutputStream("src/com/greedy/section02/preparedStatement/employee-query.xml"), "title");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// 쿼리를 파일에 담을 때 xml파일 사용.
		
		
		
	}

}
