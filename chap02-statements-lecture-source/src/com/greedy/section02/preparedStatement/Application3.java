package com.greedy.section02.preparedStatement;

import static com.greedy.common.JDBCtemplate.getConnection;
import static com.greedy.common.JDBCtemplate.close;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Application3 {

	public static void main(String[] args) {
		Connection con = getConnection();
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		Scanner sc = new Scanner(System.in);
		System.out.println("조회 하려는 사번을 입력해 주세요: ");
		String empId = sc.nextLine();
		
		String query = "SELECT EMP_ID, EMP_NAME, SALARY FROM EMPLOYEE WHERE EMP_ID = ?";
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, empId);
			// 입력된 empId값으로 ?를 채운다.
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				System.out.println(rset.getString("EMP_ID") + ", " + rset.getString("EMP_NAME") + ", " + rset.getInt("SALARY"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
			close(con);
		}
		
	}

}
