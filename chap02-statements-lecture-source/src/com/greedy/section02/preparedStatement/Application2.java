package com.greedy.section02.preparedStatement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.greedy.common.JDBCtemplate.getConnection;
import static com.greedy.common.JDBCtemplate.close;

public class Application2 {

	public static void main(String[] args) {
		Connection con = getConnection();
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		// resultset은 select 할 때만 사용한다.
		
		String empId = "208";
		
		try {
			pstmt = con.prepareStatement("SELECT EMP_ID, EMP_NAME, SALARY FROM EMPLOYEE WHERE EMP_ID = ?");
			// ? : placeholder(플레이스홀더) : 미완성 쿼리문을 만들때 더블쿼테이션을 여러번 사용안해도 된다.
			pstmt.setString(1, empId);
			// pstmt.setString의 인덱스는 1부터 시작, int값에 따라 몇번째 ?에 넣을지 말한다.
			
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				System.out.println(rset.getString("EMP_ID") + ", " + rset.getString("EMP_NAME") + ", " + rset.getInt("SALARY"));
			}
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
			close(con);
		}
		
		
	}

}
