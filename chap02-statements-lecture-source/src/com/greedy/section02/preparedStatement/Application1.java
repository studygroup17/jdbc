package com.greedy.section02.preparedStatement;
// preparedStatement : 준비된 트럭?
// 둘다 사용하는게 효율적이다. 상황별로 번갈아 가면서.
// Statement를 상속받았다.
// Statement 형으로 만든 close에 사용가능 (부모, 자식 타입이므로)
// 트럭이 만들어 지며 짐을 가진다.
// 미완성 쿼리를 사용할 때 손쉽게 사용 가능 + empId + ""
// 완성 쿼리에서는 statement 가 쉬움.

import static com.greedy.common.JDBCtemplate.getConnection;
import static com.greedy.common.JDBCtemplate.close;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Application1 {

	public static void main(String[] args) {
		Connection con = getConnection();
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		try {
			pstmt = con.prepareStatement("SELECT EMP_ID, EMP_NAME FROM EMPLOYEE");
			// prepared와 달리 d가 빠짐
			// 트럭이 만들어 지며 짐을 가진다.

			rset = pstmt.executeQuery();
			// 쿼리 내용 필요 없음. 이미 쿼리를 만들었기 때문에.
			
			while(rset.next()) {
				System.out.println(rset.getString("EMP_ID") + ", " + rset.getString("EMP_NAME"));
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
			close(con);
		}
		
	}

}
