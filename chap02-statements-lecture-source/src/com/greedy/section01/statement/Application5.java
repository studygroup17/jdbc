package com.greedy.section01.statement;


import static com.greedy.common.JDBCtemplate.getConnection;
import static com.greedy.common.JDBCtemplate.close;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.greedy.model.dto.EmployeeDTO;

/* ArrayList에 EmployeeDTO 인스턴스들을 담기(다중행 row를 자바의 하나의 인스턴스로 저장하기 ) */
public class Application5 {

	public static void main(String[] args) {
		Connection con = getConnection();
		Statement stmt = null;
		ResultSet rset = null;
		
		/* 한 행의 정보를 담을 DTO */
		EmployeeDTO row = null;
		
		/* 여러 DTO를 하나의 인스턴스로 묶기 위한 List */
		List<EmployeeDTO> empList = null;
		
		String query = "SELECT * FROM EMPLOYEE";
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			empList = new ArrayList<>();	// 전체 행(DTO 인스턴스)을 담을 컬렉션
			// 현재 empList는 null값이므로 ArrayList로 초기화
			while(rset.next()) {
				row = new EmployeeDTO();	// 한 행을 담을 DTO 인스턴스
				
				row.setEmpId(rset.getString("EMP_ID"));
				row.setEmpName(rset.getString("EMP_NAME"));
				row.setEmpNo(rset.getString("EMAIL"));
				row.setEmail(rset.getString("EMAIL"));
				row.setPhone(rset.getString("PHONE"));
				row.setDeptCode(rset.getString("DEPT_CODE"));
				row.setJobCode(rset.getString("JOB_CODE"));
				row.setSalLevel(rset.getString("SAL_LEVEL"));
				row.setSalary(rset.getInt("SALARY"));
				row.setBonus(rset.getDouble("BONUS"));
				row.setManagerId(rset.getString("MANAGER_ID"));
				row.setHireDate(rset.getDate("HIRE_DATE"));
				row.setEntDate(rset.getDate("ENT_DATE"));
				row.setEntYn(rset.getString("ENT_YN"));
				
				empList.add(row);			// 한 행을 컬렉션에 추가하자.
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(stmt);
			close(con);
		}
		
		for(EmployeeDTO emp : empList) {
			System.out.println(emp);
		}
		
		
		
	}

}
// 설정이 길어질 수록 생산과 유지보수성이 높아진다.