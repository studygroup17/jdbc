package com.greedy.section01.statement;

import static com.greedy.common.JDBCtemplate.close;
import static com.greedy.common.JDBCtemplate.getConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class Application1 {

	public static void main(String[] args) {

		Connection con = getConnection();
//		System.out.println("con 확인용: " + con);
		
		/* 쿼리문을 저장하고 실행하는 기능을 하는 용도의 인터페이스 */
		Statement stmt = null;
		// 보내는 값
		
		/* select 쿼리 실행 후 돌아오는 결과집합(Result Set)을 받아 줄 용도의 인터페이스 */
		ResultSet rset = null;
		// select시 돌아온 결과물 값
		// dml(update, insert, delete) 는 int로 돌아온다.
		
		try {
			
			/* Connection 인스턴스로 Statement 인스턴스 생성 */
			stmt = con.createStatement();
			
			rset = stmt.executeQuery("SELECT A.EMP_ID, A.EMP_NAME 사원명 FROM EMPLOYEE A");
			// executeQuery는 ResultSet 이라는 반환값을 가진다. 그래서 rset에 대입.
			// db문 작성시 세미콜론을 찍으면 안된다.
			
//			System.out.println("ResultSet 돌아오는가 " + rset);
			// @5be46f9d가 돌아옴
			while(rset.next()) {
			// 단일행일 경우 if문을 사용하기도 한다.
			// rset.next() : 다음행이 있을경우 출력
			// 현재 rset은 모든 행을 뜻한다.
				System.out.println(rset.getString("EMP_ID") + ", " + rset.getString("사원명"));
				// 현재 rset은 한 행을 뜻한다.
				// 자료형을 생각해 가며 뽑아야 한다.
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			/* 닫는 순서는 ResultSet -> Statement -> Connection 순이다. */
			close(rset);
			close(stmt);
			close(con);
			// 닫는 순서도 중요.
		}
	}

}
