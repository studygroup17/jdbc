package com.greedy.section01.statement;

import static com.greedy.common.JDBCtemplate.getConnection;
import static com.greedy.common.JDBCtemplate.close;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import com.greedy.model.dto.EmployeeDTO;

// 단일행일 경우의 예제
public class Application4 {

	public static void main(String[] args) {
		Connection con = getConnection();
		Statement stmt = null;
		ResultSet rset = null;
		
		EmployeeDTO selectedEmp = null;
		
		Scanner sc = new Scanner(System.in);
		System.out.print("조회 하려는 사번을 입력해 주세요: ");
		String empId = sc.nextLine();
		
		String query = "SELECT A.* FROM EMPLOYEE A WHERE EMP_ID = '" + empId + "'";		
		// 쿼리 자체가 문자열이라 어떤 값을 넣어도 문자열로 반환.
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
//				System.out.println(rset.getString("EMP_NAME")); 	// 한 줄의 출력문으로 현재까지 문제가 없이 동작하는지 확인
				
				selectedEmp = new EmployeeDTO();
				
				selectedEmp.setEmpId(rset.getString("EMP_ID"));
				selectedEmp.setEmpName(rset.getString("EMP_NAME"));
				selectedEmp.setEmpNo(rset.getString("EMAIL"));
				selectedEmp.setEmail(rset.getString("EMAIL"));
				selectedEmp.setPhone(rset.getString("PHONE"));
				selectedEmp.setDeptCode(rset.getString("DEPT_CODE"));
				selectedEmp.setJobCode(rset.getString("JOB_CODE"));
				selectedEmp.setSalLevel(rset.getString("SAL_LEVEL"));
				selectedEmp.setSalary(rset.getInt("SALARY"));
				selectedEmp.setBonus(rset.getDouble("BONUS"));
				selectedEmp.setManagerId(rset.getString("MANAGER_ID"));
				selectedEmp.setHireDate(rset.getDate("HIRE_DATE"));
				selectedEmp.setEntDate(rset.getDate("ENT_DATE"));
				selectedEmp.setEntYn(rset.getString("ENT_YN"));
				// selectedEmp묶음은 한명의 사원이다.
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(stmt);
			close(con);
		}
		
		System.out.println("selectedEmp : " + selectedEmp);
		
		
	}

}
