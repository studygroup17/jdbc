package com.greedy.section01.statement;

import static com.greedy.common.JDBCtemplate.close;
import static com.greedy.common.JDBCtemplate.getConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;



public class Application3 {

	public static void main(String[] args) {

		Connection con = getConnection();
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			stmt = con.createStatement();
			
			Scanner sc = new Scanner(System.in);
			System.out.print("조회 하려는 사번을 입력해 주세요: ");
			String empid = sc.nextLine();
			// db에 전달 될때는 문자로 보내지기 때문에 숫자, 문자 등 자료형태는 크게 의미가 없다.
			
			String query = "SELECT EMP_ID, EMP_NAME, SALARY FROM EMPLOYEE WHERE EMP_ID = '" + empid + "'";
			
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				System.out.println(rset.getString("EMP_ID") + ", " + rset.getString("EMP_NAME") + ", " + rset.getInt("SALARY"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(stmt);
			close(con);
		}
		
		
	}

}
