package com.greedy.section01.statement;

import static com.greedy.common.JDBCtemplate.getConnection;
import static com.greedy.common.JDBCtemplate.close;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Application2 {

	public static void main(String[] args) {

		/* 1. Connection 인스턴스 생성 */
		Connection con = getConnection();
		
		/* 2. Statement 선언 */
		// 미리 선언해야 try문 밖에서 close 가능
		Statement stmt = null;
		
		/* 3. ResultSet 선언(SELECT을 할 경우에만) */
		ResultSet rset = null;
		
		try {
			/* 4. Connection의 createStatement()를 이용해서 Statement 인스턴스 생성 */
			stmt = con.createStatement();
		
			String empid = "'207'";
//			String query = "SELECT EMP_ID, EMP_NAME, SALARY FROM EMPLOYEE WHERE EMP_ID = '207'";
			String query = "SELECT EMP_ID, EMP_NAME, SALARY FROM EMPLOYEE WHERE EMP_ID = " + empid;
			/* 5. executeQuery()로 쿼리문을 실행하고 결과를 ResultSet으로 반환 받음 */
			rset = stmt.executeQuery(query);
			
			/* 6. ResultSet에 담긴 결과물을 컬럼이름(순번, 별칭)을 이용해 꺼내오기 */
			if(rset.next() ) {				// 조회되는 ResultSet이 단일행일 경우에는 if문을 쓴다. (while 해도 동작은 함)
				System.out.println(rset.getString("EMP_ID") + ", " + rset.getString("EMP_NAME")
									+ ", " + rset.getInt("SALARY"));
			} // 1회 반복 마다 해당 행의 컬럼값을 get으로 가져온다.
		
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			/* 7. 사용한 자원 반납. */
			close(rset);
			close(stmt);
			close(con);
			
			
		}
		
	}

}
