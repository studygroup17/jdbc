package com.greedy.section01.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;
import com.greedy.section01.model.service.OrderService;

public class OrderMenu {
	
	private OrderService orderService = new OrderService();
	// mvc 구조는 순서가 정확히 순서가 부여되어 있기 때문에 무조건 view다음에 service가 오게끔 만들기 위해 필드서 생성.
	
	public void displayMainMenu() {
		
		/*
		 * 반복
		 * --------------------------------------------------
		 * 1. 카테고리 조회
		 * 2. 해당 카테고리의 메뉴 조회
		 * 3. 사용자에게 어떤 메뉴를 주문받을 것인지 입력
		 * 4. 주문 할 수량 입력
		 * --------------------------------------------------
		 * 5. 주문
		 */
		Scanner sc = new Scanner(System.in);
		
		int totalOrderPrice = 0;								// 사용자가 고른 메뉴와 수량에 따른 주문 한건에 대한 총 금액을 저장할 변수
		List<OrderMenuDTO> orderMenuList = new ArrayList<>();	// 사용자가 고른 메뉴와 수량(OrderMenuDTO)들을 저장할 List
		
		boolean flag = true;
		do {
			System.out.println("============ 음식 주문 프로그램 ============");
			
			// DAO까지 클래스명 빼고는 계속 같은 구문으로사용된다.
			/* DB의 실시간으로 존재하는 카테고리 조회 후 화면에 출력 */
			List<CategoryDTO> categoryList = orderService.SelectAllCategory();
			
			for(CategoryDTO cate : categoryList) {
				System.out.println(cate.getName());
			}
			
			/* 주문 할 카테고리 선택 */
			System.out.println("=======================================");
			System.out.println("주문하실 카테고리 종류를 입력해 주세요: ");
			String inputCategory = sc.nextLine();
			
			/* 사용자가 입력한 카테고리의 코드 번호 확인(가공) */
			int categoryCode = 0;
			for(int i = 0; i < categoryList.size(); i++) {
				CategoryDTO category = categoryList.get(i);
				if(category.getName().equals(inputCategory)) {
					categoryCode = category.getCode();
					break;
				}
			/*
			 * 해석: 카테고리list 안에있는 DTO하나하나가 하나의 행이다.
			 * 		각 행들의 카테고리명을 뽑아내 사용자가 입력한 값과 일치하는 값이 있을 경우 
			 * 		해당 행의 code를 categoryCode에 담는다.
			 * 		break : 찾았을 경우 뒤의 행들도 반복되지 않도록 정지
			 */
			}
			
			/* 사용자가 입력한 카테고리에 해당하는 메뉴, 가격 조회 */
			System.out.println("============ 주문 가능 메뉴 ============");
			// 사용자로부터 입력받은 값을 전달인자로 사용.
			List<MenuDTO> menuList = orderService.SelectMenuBy(categoryCode);
			
			for(MenuDTO menu : menuList) {
				System.out.println(menu.getName() + ", " + menu.getPrice());
			}
			
			/* 사용자가 메뉴를 고르고 메뉴 코드 번호 및 가격 확인 */
			System.out.print("주문하실 메뉴를 선택해 주세요: ");
			String inputMenu = sc.nextLine();
			
			int menuCode = 0;
			int menuPrice = 0;
			for(int i = 0; i < menuList.size(); i++) {
				MenuDTO menu = menuList.get(i);
				if(menu.getName().equals(inputMenu)) {
					menuCode = menu.getCode();
					menuPrice = menu.getPrice();
					break;
					// pk,fk로 사용된 메뉴 코드와, 나중에 총 주문 가격을 알기위해 추출.
				}
			}
			
			/* 해당 메뉴를 얼만큼 주문할 지 수량 확인 */
			System.out.println("주문하실 수량을 입력하세요: ");
			int orderAmount = sc.nextInt();
			
			OrderMenuDTO orderMenu = new OrderMenuDTO();	// OrderMenuDTO는 주문 시 하나의 메뉴에 대한 정보가 담기는 DTO
			orderMenu.setMenuCode(menuCode);
			orderMenu.setAmount(orderAmount);
			// 트랜잭션이 가장 많은 상황(은행 입/출금 & 주문)
			
			/* 하나의 메뉴에 대한 총금액을 알 수 있으므로 전체 총금액 변수에 누적하자 */
			totalOrderPrice += menuPrice * orderAmount;
			
			/* 하나의 메뉴에 대한 내용을 orderMenuList에 추가 */
			orderMenuList.add(orderMenu);
			
			
			/* 메뉴 반복 여부 판단 */
//			System.out.println("계속 주문하시곗습니까? (예/아니오): ");
//			sc.nextLine();
//			// 바로 위에 nextInt를 사용했기 때문
//			boolean isContinue = sc.nextLine().equals("예") ? true : false;
//			
//			if(!isContinue) {	// "예"를 선택하지 않았을 때 메뉴 반복을 멈추게 하자.
//				break;
//			}
			
			/* "예"는 다시 반복, "아니오"는 반복 중지, "그 외의 값"은 다시 계속 주문하시겠습니까?를 물어보게 해보자 */
			sc.nextLine();
			String isContinue = "";
			while(true) {
				System.out.println("계속 주문하시곗습니까? (예/아니오): ");
				isContinue = sc.nextLine();
				if("예".equals(isContinue)) {
					break;
				} else if("아니오".equals(isContinue)) {
					flag = false;
					break;
				}
					
			}
			
			
		} while(flag);
		
		/* 사용자가 고른 메뉴들에 대한 전체 내용 출력 */
		// 사용자가 아니오 누른순간 모든게 처리되어야 한다.
		// 즉 반복할 때 마다 사용자 주문을 누적시켜놔야 한다.
		System.out.println("============ 총 주문 내용 ============");
		for(OrderMenuDTO orderMenu : orderMenuList) {
			System.out.println(orderMenu);
		}
		
		/* 사용자가 고른 메뉴들에 대한 총 주문 금액 */
		System.out.println("총 주문 금액: " + totalOrderPrice);
	}

}
