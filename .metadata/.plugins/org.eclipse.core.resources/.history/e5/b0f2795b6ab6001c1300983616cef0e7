package com.greedy.section01.model.dao;

import static com.greedy.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;

public class OrderDAO {

	private Properties prop = new Properties();
	
	public OrderDAO() {
		try {
			prop.loadFromXML(new FileInputStream("mapper/order-query.xml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/* 카테고리 전체 조회용 메소드 */
	public List<CategoryDTO> selectAllCategory(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		
		List<CategoryDTO> categoryList = null;
		
		String query = prop.getProperty("selectAllCategory");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			categoryList = new ArrayList<>();
			while(rset.next()) {
				// 카테고리 번호와 명만 필요하기 때문에 두개만 뽑는다.
				
				CategoryDTO category = new CategoryDTO();
				category.setCode(rset.getInt("CATEGORY_CODE"));
				category.setName(rset.getString("CATEGORY_NAME"));
				
				categoryList.add(category);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(stmt);
		}
		
		// view까지 계속 return된다.
		return categoryList;
	}

	public List<MenuDTO> SelectMenuBy(Connection con, int categoryCode) {
		PreparedStatement pstmt = null;
		// pstmt는 사용자 입력값을 사용할 때 활용
		ResultSet rset = null;
		// rset은 받을 행이 있을 경우
		
		List<MenuDTO> menuList = null;
		
		String query = prop.getProperty("selectMenuByCategory");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, categoryCode);
			// why? :
			
			menuList = new ArrayList<>();
			rset = pstmt.executeQuery();
			while(rset.next()) {
				MenuDTO menu = new MenuDTO();
				menu.setCode(rset.getInt("MENU_CODE"));
				menu.setName(rset.getString("MENU_NAME"));
				menu.setPrice(rset.getInt("MENU_PRICE"));
				menu.setCategoryCode(rset.getInt("CATEGORY_CODE"));
				menu.setOrderableStatus(rset.getString("ORDERABLE_STATUS"));
				// 만약 get(자료형)이 헷갈린다면 DTO에 set(명칭)과 맞추면 된다.
				
				menuList.add(menu);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		

		return menuList;
	}
}
