package com.greedy.section02.template;

/* 일반적인 import는 클래스명까지 */
import java.sql.Connection;
import java.sql.SQLException;

/* import static은 메소드명까지 */
import static com.greedy.section02.template.JDBCTemplate.getConnection;
// static을 import
import static com.greedy.section02.template.JDBCTemplate.close;

public class Application {

	public static void main(String[] args) {
//		Connection con = com.greedy.section02.template.JDBCTemplate.getConnection();
//		System.out.println("main에서의 con: " + con);
		
		/* 같은 패키지의 클래스이므로 */
//		Connection con = JDBCTemplate.getConnection();
//		System.out.println("main에서의  con: " + con);
	
		/* import static 방식으로 클래스명까지 생략 함*/
		Connection con = getConnection();
		System.out.println("main에서의 con: " + con);
		
		/* Connnection 인스턴스를 활용해서 DB와의 CRUD를 위한 비즈니스 로직 실행(Connection 인스턴스를 사용) */
		
		/* Connection 인스턴스 반납 시 일일히 try - catch로 감싸 주어야 한다. */
//		try {
//			con.close();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
	
		close(con);
		
	}

}
// 비즈니스 로직이라 한다.
// 공통 모듈로 빼내는 과정.
// 주된 관심사만 볼 수 있다.

// 에러를 잡고 싶으면 구간 별로 출력문을 작성해 위치 파악.