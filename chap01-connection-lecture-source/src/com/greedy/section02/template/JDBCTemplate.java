package com.greedy.section02.template;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/* JDBC를 진행할 때 자주 쓰이는 기능을 공통 모듈로 분리 */
/* Connection 객체 만들고 close 관련 기능 담당 */
// jdbct에 공통적으로 사용되는 기능을 빼서 메소드로 만드는 클래스 //

public class JDBCTemplate {
	
	// 공통 모듈이기 때문에 public static
	// 싱글톤은 공유를 활용한 하나의 인스턴스
	// 해당 public static은 static영역의 모두가 가져다 사용
	public static Connection getConnection() {
		
		Connection con = null;
		
		Properties prop = new Properties();
		
		try {
			prop.load(new FileReader("config/connection-info.properties"));
			
//			System.out.println(prop);
			String driver = prop.getProperty("driver");
			String url = prop.getProperty("url");
			
			Class.forName(driver);
			
			con = DriverManager.getConnection(url, prop); // 확인용
			// con = DriverManager.getConnection(url,user, password); 가 기본 방식이지만.
			// (url, prop)으로 사용하고 싶으면prop안에 키 값이 user, password로 지정되어 있어야 된다. 
			// 그러면 getConnection이 알아서 꺼내 사용
			 
			System.out.println("getConnection에서의 con: " + con); // 확인용
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return con;
	} // 커넷션 객체를 열기위해 반환하는 메소드.(닫으면 안된다.)

	/* Connection 인스턴스를 닫아주는 메소드(예외처리를 대신 하는 용도) */
	public static void close(Connection con) {
		try {
			if(con != null && !con.isClosed()) {	// 넘어 온 Connection 인스턴스가 존재하고 닫혀있지 않을 때
			// isClosed() : 닫혀 있다면 true를 반환.
			// Connection 인스턴스 자체가 스트림 이라 할 수 있다.
			con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
// config 폴더 생성
// 어떤 패키지든 사용 가능한 파일 위치
// connection 모든 정보를 가지고 있는 config파일 생성

// ctrl + H 시 내가 작성한 부분을 serch 할 수 있다. 
// println을 찾을때 어떤 내용인지 모를 수 있기 때문에 println("1번째 print:" + num)으로 달아야 찾기 쉽다.