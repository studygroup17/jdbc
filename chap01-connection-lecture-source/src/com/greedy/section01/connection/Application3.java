package com.greedy.section01.connection;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Application3 {

	public static void main(String[] args) {

		Properties prop = new Properties();
		// 일반 class라서 new 생성자
		
		Connection con = null;
		// interface여서 null로 선언.
		
		try {
			prop.load(new FileReader("src/com/greedy/section01/connection/jdbc-config.properties"));
			
			System.out.println(prop);
			
			String driver = prop.getProperty("driver");
			String url = prop.getProperty("url");
			String user = prop.getProperty("user");
			String password = prop.getProperty("password");
			
			Class.forName(driver);
			
			con = DriverManager.getConnection(url, user, password);
			// 출력시 물리적인 시간이 걸린다 (확인이 필요하기 때문에)
			
			System.out.println("con: " + con);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
//map부분 properties 듣기
//FILE은 띄어쓰기시 오류 발생한다.
// 현재 작성한 커넥트를 재사용 할 수 있게 한다. -> 공통 모듈로 빼낸다 라고 한다.