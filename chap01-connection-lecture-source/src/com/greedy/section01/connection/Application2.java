package com.greedy.section01.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Application2 {

	public static void main(String[] args) {

		String driver = "oracle.jdbc.driver.OracleDriver";
		String url = "jdbc:oracle:thin:@localhost:1521:xe";
		// 127.0.0.1 대신 localhost 로 사용 가능하다.
		String user = "C##EMPLOYEE";
		String password = "EMPLOYEE";
		
		// 해당 부분을 파일로 뺴낼것.
		// 수정이 편리(자바코드에서 수정시 적용시간이 오래걸린다.) , 한눈에 알아보기 쉽다.
		// 파일을 패키지 및으로 저장하면 다른 패키지에서 해당 패키지를 사용하는 것으로
		// 공통적인 모듈이라는 말과 맞지 않는다. 그래서 프로젝트 바로 밑에 파일을 만든다.
		
		Connection con = null;
		
		try {
			Class.forName(driver);
			
			con = DriverManager.getConnection(url, user, password);
			
			System.out.println("con: " + con);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		
	}

}
