package com.greedy.section01.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Application1 {

	public static void main(String[] args) {

		/*
		 * DB 접속을 위한 Connection 인스턴스 생성을 위해 레퍼런스 변수 선언
		 * (나중에 finally 블럭에서 사용하기 위해서 try-catch블럭 밖에서 선언)
		 */
		Connection con = null;
		
		
		try {
			
			/* 사용 할 드라이버 등록(사용 할 DBMS 인지) */
			Class.forName("oracle.jdbc.driver.OracleDriver");
			// ojdbc8 라이브러리에 있는 클래스 이름.
			// ojdbc8을 라이브러리에 등록해야 사용 가능.
			
			/* DriverManager를 이용해 Connection 인스턴스 생성 */
			con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe",
											  "C##EMPLOYEE", "EMPLOYEE"); // 계정이 아이디와 비밀번호 대소문자 지키기
			// 1521포트번호를 쓰는 내컴퓨터에 있는 xe을 사용 하겠다.
			// 자바와 EMPLOYEE계정과의 CONNECTION
			
			System.out.println("con: " + con);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
// con: oracle.jdbc.driver.T4CConnection@6743e411 가 나오면 connection이 완료되어 객체 주고값이 나온 것.
// close는 요청이 있을 때마다 열었다 닫아야 한다.
