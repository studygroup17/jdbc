package com.greedy.section02.update;

import static com.greedy.common.JDBCTemplate.getConnection;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

import com.greedy.common.model.dto.MenuDTO;
import static com.greedy.common.JDBCTemplate.close;


public class Application1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("변경 할 메뉴 번호를 입력하세요: ");
		int menuCode = sc.nextInt();
		// pk값으로 수정할 메뉴를 고르기 위해 입력 받는다.
		System.out.print("변경 할 메뉴의 이름을 입력하세요: ");
		sc.nextLine();
		String menuName = sc.nextLine();
		System.out.print("변경 할 메뉴의 가격을 입력하세요: ");
		int menuPrice = sc.nextInt();
		System.out.print("변경 할 메뉴의 카테고리를 입력하세요(4~12): ");
		int categoryCode = sc.nextInt();
		System.out.print("메뉴 판매 여부를 입력하세요(Y/N): ");
		sc.nextLine();
		String orderableStatus = sc.nextLine().toUpperCase();
		
		MenuDTO changedMenu = new MenuDTO();
		changedMenu.setCode(menuCode);
		changedMenu.setName(menuName);
		changedMenu.setPrice(menuPrice);
		changedMenu.setCatergoryCode(categoryCode);
		changedMenu.setOrderableStatus(orderableStatus);
		
//		---------------------------------------------------------------------------------
//		다른 테이블 가정
		
		Connection con = getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		
		Properties prop = new Properties();
		try {
			prop.loadFromXML(new FileInputStream("mapper/menu-query.xml"));
			String query = prop.getProperty("updateMenu");
			
//			System.out.println("query" + query);  // 확인 구간 // 확인 완료
			
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, changedMenu.getName());
			pstmt.setInt(2, changedMenu.getPrice());
			pstmt.setInt(3, changedMenu.getCatergoryCode());
			pstmt.setString(4, changedMenu.getOrderableStatus());
			pstmt.setInt(5, changedMenu.getCode());
			
			result = pstmt.executeUpdate();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(con);
		}
		
		
		if(result > 0) {
			System.out.println("메뉴 수정 완료");
		} else {
			System.out.println("메뉴 수정 오류");
		}
	}

}
