package com.greedy.section01.insert;

import static com.greedy.common.JDBCTemplate.close;
import static com.greedy.common.JDBCTemplate.getConnection;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

public class Application2 {
// 현재 쿼리를 기반으로 짜기 때문에 PreparedStatement형을 사용. int result 사용
	public static void main(String[] args) {
		Connection con = getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		
		Properties prop = new Properties();
		
		try {
			prop.loadFromXML(new FileInputStream("mapper/menu-query.xml"));
			String query = prop.getProperty("insertMenu");
//			System.out.println(query); 확인. 이상무.
			
			Scanner sc = new Scanner(System.in);
			System.out.println("메뉴의 이름을 입력하세요: ");
			String menuName = sc.nextLine();
			System.out.println("메뉴의 가격을 입력하세요: ");
			int menuPrice = sc.nextInt();
			System.out.println("메뉴의 코드를 입력하세요(4~12): ");
			int categoryCode = sc.nextInt();
			System.out.println("판매 여부를 결정해 주세요: ");
			sc.nextLine(); // 다시 nextLine을 사용하기 때문에 엔터 먹기
			String orderableStatus = sc.nextLine().toUpperCase();
			// toUpperCase : 소문자로 입력해도 대문자로 바뀌게 설정 (Y, N만 들어가도록)
			
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, menuName);
			pstmt.setInt(2, menuPrice);
			pstmt.setInt(3, categoryCode);
			pstmt.setString(4, orderableStatus);
			
			result = pstmt.executeUpdate();
			
			System.out.println("result: " + result);
			
			
		
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(con);
		}
		
		if(result > 0) {
			System.out.println("메뉴 등록 성공!");	// 정상흐름이라 한다.
		} else {
			System.out.println("메뉴 등록 실패");	// 예외흐름이라 한다. (회원가입시 아이디가 정상적이지 않을 경우)
		}
	}

}
