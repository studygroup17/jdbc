package com.greedy.section01.insert;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import static com.greedy.common.JDBCTemplate.getConnection;
import static com.greedy.common.JDBCTemplate.close;

public class Application1 {

	public static void main(String[] args) {
		Connection con = getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		// DML을 진행하면 결과과 ResultSet이 아닌 int가 반환된다.
		
		Properties prop = new Properties();
		
		try {
			prop.loadFromXML(new FileInputStream("mapper/menu-query.xml"));
			
			String query = prop.getProperty("insertMenu");
			
//			System.out.println("query : " + query); 확인
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, "초콜릿샤브샤브");
			pstmt.setInt(2, 100000);
			pstmt.setInt(3, 7);
			pstmt.setString(4, "Y");
			result = pstmt.executeUpdate();
			System.out.println("result :" + result);
//			pstmt.execute()가 왜 안되지? => pstmt.executeUpdate() : DML쿼리를 수행시 executeUpdate()이며 결과는 int다.
//			int 안에는 DML작업이 이뤄진 행의 갯수인 1이 반환된다.
//			query문 안에 ?가 4개 이다.
//			전부 setString해도 되지만 DTO 필드값에 넣을 것 이기 때문에 수정
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(con);			// 트랜잭션인 commit 발생
//			rset은 INT형을 썻기 때문에 close할 필요가 없다.
		}
		
		
		
		
		
	}

}
