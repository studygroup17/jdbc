package com.greedy.section01.insert;

import java.awt.Menu;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

import com.greedy.common.model.dto.MenuDTO;
import static com.greedy.common.JDBCTemplate.getConnection;
import static com.greedy.common.JDBCTemplate.close;



public class Application3 {

	public static void main(String[] args) {
		
		/* 다른 쪽 클래스(View)에서 작성한다고 가정 */
		Scanner sc = new Scanner(System.in);
		System.out.print("메뉴의 이름을 입력하세요: ");
		String menuName = sc.nextLine();
		System.out.print("메뉴의 가격을 입력하세요: ");
		int menuPrice = sc.nextInt();
		System.out.print("카테고리 코드를 입력하세요(4~12): ");
		int categoryCode = sc.nextInt();
		System.out.print("판매 여부를 결정해 주세요(Y/N): ");
		sc.nextLine();
		String orderableStatus = sc.nextLine().toUpperCase();
		
//		MenuDTO newMenu = new MenuDTO(menuName, menuPrice, categoryCode, orderableStatus)); 불가
//		왜? 매개변수 5개 짜리 menuDTO를 만들었기 때문에 4개로 불가
//		때문에 오버로딩해야 하는 번거로움. 그래서 setter 사용
		
		MenuDTO newMenu = new MenuDTO();
		newMenu.setName(menuName);
		newMenu.setPrice(menuPrice);
		newMenu.setCatergoryCode(categoryCode);
		newMenu.setOrderableStatus(orderableStatus);
		
		
//		-----------------------------------------------
//		임의의 다른 클래스
//		가공 처리
		
		Connection con = getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		Properties prop = new Properties();
		
//		DTO에 넣은 값을 get으로 꺼내 사용
		try {
			prop.loadFromXML(new FileInputStream("mapper/menu-query.xml"));
			String query = prop.getProperty("insertMenu");
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, newMenu.getName());
			pstmt.setInt(2, newMenu.getPrice());
			pstmt.setInt(3, newMenu.getCatergoryCode());
			pstmt.setString(4, newMenu.getOrderableStatus());
			result = pstmt.executeUpdate();
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(con);
		}
		
		if(result > 0) {
			System.out.println("메뉴 등록 성공");
		} else {
			System.out.println("메뉴 등록 실패");
		}
	}

}
