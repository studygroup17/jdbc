package com.greedy.common.model.dto;

import java.io.Serializable;

public class MenuDTO implements Serializable{
	// DTO는 row의 의미를 지닌다. (한 행)
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5129216454773409277L;
	// 시리얼버전 UID라고 부른다.
	
	private int code;
	// 이미 menuDTO안에 menu라는 정보가 있기 때문에 menu를 사용하지 않을 수 있다.
	private String name;
	private int price;
	private int catergoryCode;
	private String orderableStatus;
	
	public MenuDTO() {
	}

	public MenuDTO(int code, String name, int price, int catergoryCode, String orderableStatus) {
		this.code = code;
		this.name = name;
		this.price = price;
		this.catergoryCode = catergoryCode;
		this.orderableStatus = orderableStatus;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getCatergoryCode() {
		return catergoryCode;
	}

	public void setCatergoryCode(int catergoryCode) {
		this.catergoryCode = catergoryCode;
	}

	public String getOrderableStatus() {
		return orderableStatus;
	}

	public void setOrderableStatus(String orderableStatus) {
		this.orderableStatus = orderableStatus;
	}

	@Override
	public String toString() {
		return "MenuDTO [code=" + code + ", name=" + name + ", price=" + price + ", catergoryCode=" + catergoryCode
				+ ", orderableStatus=" + orderableStatus + "]";
	}
	
	
	
	
	
}
