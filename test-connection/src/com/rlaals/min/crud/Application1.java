package com.rlaals.min.crud;

import static com.greedy.min.common.JDBCTemplate.getConnection;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

import com.rlaals.min.common.dto.MenuDTO;
import static com.greedy.min.common.JDBCTemplate.close;

public class Application1 {
	
	public static void main(String[] args) {
		
//		1. 기본 커넥션 선언 구문 작성
		Connection con = getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		
//		2. 조건 query를 불러올 prop 객체 생성
		Properties prop = new Properties();
		
		try {
//			3. insert query load 및 query변수 대입
			prop.loadFromXML(new FileInputStream("mapper/crud-query.xml"));
			String query = prop.getProperty("insertquery");
			
//			4. DTO 객체를 생성해 한 행에 들어갈 입력값 담기
			MenuDTO menu = new MenuDTO();
			
			Scanner sc = new Scanner(System.in);
			System.out.println("음식명을 입력하세요: ");
			menu.setName(sc.nextLine());
			System.out.println("음식가격을 입력하세요: ");
			menu.setPrice(sc.nextInt());
			System.out.println("카테고리를 입력하세요: ");
			menu.setCategoryCode(sc.nextInt()) ;
			System.out.println("판매유무를 입력하세요: ");
			sc.nextLine();
			menu.setOrderableStatus(sc.nextLine().toUpperCase());
			
//			5. placeholder을 담은 query문 prepareStatement 생성
			pstmt = con.prepareStatement(query);
			
//			6. DTO에 담은 값들을 뽑아내 placeholder에 순차적 대입
			pstmt.setString(1, menu.getName());
			pstmt.setInt(2, menu.getPrice());
			pstmt.setInt(3, menu.getCategoryCode());
			pstmt.setString(4, menu.getOrderableStatus());
			
//			7. DB에 query 전달 및 반환값 저장.
			result = pstmt.executeUpdate();
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
//			8. 자원 낭비 방지 및 commit을 위한 close
			close(pstmt);
			close(con);
		}
		
//		9. 정상적으로 insert 되었는지 확인하기 위한 출력문 
		if(result > 0) {
			System.out.println("정상적으로 행 추가");
		} else {
			System.out.println("행 추가 실패");
		}
	}

}
