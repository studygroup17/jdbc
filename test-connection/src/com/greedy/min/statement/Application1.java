package com.greedy.min.statement;

import static com.greedy.min.common.JDBCTemplate.getConnection;
import static com.greedy.min.common.JDBCTemplate.close;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Application1 {

	public static void main(String[] args) {

		Connection con = getConnection();
		// Connection 생성 메소드 실행 및 인스턴스화
		Statement stmt = null;
		// Statement 선언
		ResultSet rset = null;
		// ResultSet 선언
		
		try {
			stmt = con.createStatement();
			// Connection의 createStatement() 메소드를 사용해 Statement 생성
			
			Scanner sc = new Scanner(System.in);
			System.out.println(" D1, D2, D3, D4, D5, D6, D7, D8, D9 ");
			System.out.print("원하는 부서 코드를 입력하시오: ");
			String dept = sc.nextLine();
			// 부서 코드 입력값을 받아 dept 변수에 대입
			
			rset = stmt.executeQuery("SELECT EMP_ID 별칭, EMP_NAME, EMP_NO, DEPT_CODE, JOB_CODE FROM EMPLOYEE WHERE DEPT_CODE IN('" + dept + "')");
			// 쿼리문 작성해 DB에 쿼리문 전달 및 실행 & 반환값 rset 대입
			while(rset.next()) {
				System.out.println(rset.getString("별칭") + ", " + rset.getString("EMP_NAME") + ", " + rset.getString("DEPT_CODE"));
			}
			// 반복문 활용 각 행별 EMP_ID, EMP_NAME, DEPT_CODE 출력
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(stmt);
			close(con);
			// 닫기
		}
	}

}
