package com.greedy.min.common;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class JDBCTemplate {

	public static Connection getConnection() {
		// CONNECTION 인스턴스 생성 공용 모듈
		Connection con = null;
		Properties prop = new Properties();
		try {
			prop.load(new FileReader("config/connection-info.properties"));
			// 파일에 저장되어 있는 값들 INPUT
			
			String driver = prop.getProperty("driver");
			String url = prop.getProperty("url");
			
			Class.forName(driver);
			// 드라이버 등록
			con = DriverManager.getConnection(url, prop);
			// DBMS 연결
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
	
	public static void close(Connection con) {
		// Connection CLOSE 공용모듈
		try {
			if(con != null || !con.isClosed()) {
			con.close();
			// 정상적으로 연결되어 있고 닫혀있지 않을 때 close
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void close(Statement stmt) {
		// Statement CLOSE 공용모듈
		try {
			if(stmt != null || !stmt.isClosed()) {
			stmt.close();
			// 정상적으로 연결되어 있고 닫혀있지 않을 때 close
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void close(ResultSet rset) {
		// ResultSet CLOSE 공용모듈
		try {
			if(rset != null || !rset.isClosed()) {
			rset.close();
			// 정상적으로 연결되어 있고 닫혀있지 않을 때 close
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
