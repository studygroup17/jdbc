package com.greedy.min.statementTest;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class mxlCreate {

	public static void main(String[] args) {
		Properties prop = new Properties();
		prop.setProperty("key1", "value1");
		
		try {
			prop.storeToXML(new FileOutputStream("src/com/greedy/min/statementTest/testXml.xml"), null);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
