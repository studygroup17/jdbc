package com.greedy.min.statementTest;

import static com.greedy.min.common.JDBCTemplate.getConnection;
import static com.greedy.min.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import com.greedy.min.model.EmployeeDTO;

public class Application1 {

	public static void main(String[] args) {
//		사용할 인스턴스 선언
//		기본 connection을 위한 선언
		Connection con = getConnection();
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
//		다중행을 위한 선언
		EmployeeDTO empDTO = null;
		
//		다중행을 담아 출력하거나 사용하기 위한 선언
		List<EmployeeDTO> empList = new ArrayList<>();
		
//		?값에 지정해줄 변수 scanner : salarySample에 담는다.
		Scanner sc = new Scanner(System.in);
		System.out.print("기준 월급값을 입력하시오: ");
		int salarySample = sc.nextInt();
		
//		select 조회문을 기록한 파일을 불러 사용하기위한 선언
		Properties prop = new Properties();
		
		try {
//			xml형식 파일에 담은 쿼리문을 load 해온다.
			prop.loadFromXML(new FileInputStream("src/com/greedy/min/statementTest/testXml.xml"));
//			xml에 key1값으로 지정해둔 쿼리 value를 불러 pstmt statement에 담는다.
			pstmt = con.prepareStatement(prop.getProperty("key1"));
//			placeholder 값에 salarySample 변수를 지정한다.
			pstmt.setInt(1, salarySample);
//			pstmt 쿼리 구문을 DB에 전달하고 반환값을 rset에 담는다.
			rset = pstmt.executeQuery();
			
//			담긴 다중행을 출력하기 위해 while(반복문)을 사용해 empDTO에 넣고, List에 한 행씩(empDTO) 저장
			while(rset.next()) {
				empDTO = new EmployeeDTO();
				
				empDTO.setEmpId(rset.getString("EMP_ID"));
				empDTO.setEmpName(rset.getString("EMP_NAME"));
				empDTO.setEmpNo(rset.getString("EMP_NO"));
				empDTO.setEmail(rset.getString("EMAIL"));
				empDTO.setPhone(rset.getString("PHONE"));
				empDTO.setDeptCode(rset.getString("DEPT_CODE"));
				empDTO.setJobCode(rset.getString("JOB_CODE"));
				empDTO.setSalLevel(rset.getString("SAL_LEVEL"));
				empDTO.setSalary(rset.getInt("SALARY"));
				empDTO.setBonus(rset.getDouble("BONUS"));
				empDTO.setManagerId(rset.getString("MANAGER_ID"));
				
				empList.add(empDTO);
				
			}
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (InvalidPropertiesFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// 자원낭비를 막기위해 열어놓은 공간을 닫는다. 순서는 선언 순서의 역방향
			close(rset);
			close(pstmt);
			close(con);
		}
		
		// List에 담은 값들을 전부 출력
		// 다중행 출력 방법은 for문, for~each, iterater, toString 방식이 있다.
		for(EmployeeDTO emp : empList) {
			// empList가 에러 표시난다.
			// >: List선언이 try 필드 안에서 행해지고 있어 그렇다. 
			// 메인 메소드 가장 밖에서 선언하기
			System.out.println(emp);
		}
		
	}

}