package com.greedy.min.statementTest;

import static com.greedy.min.common.JDBCTemplate.getConnection;
import static com.greedy.min.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import com.greedy.min.model.EmployeeDTO;

public class test4 {

	public static void main(String[] args) {
		Connection con = getConnection();
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		EmployeeDTO empDTO = null;
		List<EmployeeDTO> empList = new ArrayList<>();
		
		Scanner sc = new Scanner(System.in);
		System.out.print("기준 월급값을 입력하시오: ");
		int salarySample = sc.nextInt();
		Properties prop = new Properties();
		
		try {
			prop.loadFromXML(new FileInputStream("src/com/greedy/min/statementTest/testXml.xml"));
			pstmt = con.prepareStatement(prop.getProperty("key1"));
			pstmt.setInt(1, salarySample);
			rset = pstmt.executeQuery();
			
			while(rset.next()) {
				empDTO = new EmployeeDTO();
				
				empDTO.setEmpId(rset.getString("EMP_ID"));
				empDTO.setEmpName(rset.getString("EMP_NAME"));
				empDTO.setEmpNo(rset.getString("EMP_NO"));
				empDTO.setEmail(rset.getString("EMAIL"));
				empDTO.setPhone(rset.getString("PHONE"));
				empDTO.setDeptCode(rset.getString("DEPT_CODE"));
				empDTO.setJobCode(rset.getString("JOB_CODE"));
				empDTO.setSalLevel(rset.getString("SAL_LEVEL"));
				empDTO.setSalary(rset.getInt("SALARY"));
				empDTO.setBonus(rset.getDouble("BONUS"));
				empDTO.setManagerId(rset.getString("MANAGER_ID"));
				
				empList.add(empDTO);
				
			}
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (InvalidPropertiesFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
			close(con);
		}
		
		for(EmployeeDTO emp : empList) {
			// empList가 에러 표시난다.
			// >: List선언이 try 필드 안에서 행해지고 있어 그렇다. 
			// 메인 메소드 가장 밖에서 선언하기
			System.out.println(emp);
		}
		
	}

}
