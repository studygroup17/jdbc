package algorithm.com.justmine1227.backjoon.easy.num10828;
import java.io.*;
import java.util.*;
public class Main5 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		Stack<Integer> stack = new Stack<Integer>();
		
		int num = Integer.parseInt(br.readLine());
		
		for(int i = 0; i < num; i++) {
			String[] input = br.readLine().split(" ");
			switch(input[0]) {
				case "push" : 
					stack.push(Integer.parseInt(input[1]));
					break;
				case "pop" : 
					if (stack.empty()) {
                        bw.write("-1" + "\n");
                    } else {
                        bw.write(stack.pop() + "\n");
                    }
					break;
				case "size" : 
					bw.write(stack.size() + "\n");
					break;
				case "empty" : 
					if(stack.empty()) {
						bw.write("1" + "\n");
					} else {
						bw.write("0" + "\n");
					}
					break;
				case "top" : 
					if(stack.empty()) {
						bw.write("-1" + "\n");
					} else {
						bw.write(stack.peek() + "\n");
					}
					break;
				default :
					i--;
					break;
			}
		}
		bw.flush();
		bw.close();
	}
}
// 문자 입력 받을 때 1라인에 띄어 쓰기 구분은 어떻게 입력 받아야 하지?
// push일 경우에만 추가 숫자가 있으니 if문으로 입력값이 push인 경우만 골라내자