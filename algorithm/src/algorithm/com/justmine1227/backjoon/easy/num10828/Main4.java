package algorithm.com.justmine1227.backjoon.easy.num10828;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.*;
public class Main2 {
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		Stack<Integer> stack = new Stack<Integer>();
		
		int num = Integer.parseInt(br.readLine());
		
		for(int i = 0; i < num; i++) {
			String[] input = br.readLine().split(" ");
			switch(input[0]) {
				case "push" : 
					stack.push(Integer.parseInt(input[i]));
					break;
				case "pop" : 
					stack.pop();
					break;
				case "size" : 
					stack.size();
					break;
				case "empty" : 
					stack.empty();
					break;
				case "top" : 
					stack.peek();
					break;
				default :
					i--;
					break;
			};
		}
		bw.flush();
		bw.close();
	}
}
// 문자 입력 받을 때 1라인에 띄어 쓰기 구분은 어떻게 입력 받아야 하지?
// push일 경우에만 추가 숫자가 있으니 if문으로 입력값이 push인 경우만 골라내자