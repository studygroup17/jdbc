package com.greedy.section01.run;

import com.greedy.section01.view.OrderMenu;

public class Application {

	public static void main(String[] args) {

		OrderMenu orderMenu = new OrderMenu();
		// orderMenu를 호출하면 prop생성 > dao생성 > service > view 순서로 생성된다.
		//      왜? 생성자가 생성되기 전에 필드가 먼저 실행되기 때문에 호출되면 DB까지 통로가 생성되는 것
		// 즉, 인스턴스가 생성되기 전 다음 순서로 올 계층의 인스턴스를 미리 가지고 있는다. 
		/* mvc 구조는 각자 역할이 정해져 있고 DB까지 계층 순서가 정해져 있기 때문에
		 * 생성하자마자 다음 계층을 바로 사용할 수 있도록 미리 지정해두는 것
		 */ 
		// 방향이 정해져 있어 스파게티 코딩을 막음
		// 처음 설정시 시간이 걸리지만 이후로는 DB까지 작성 시간을 줄여준다. + (코드 간소화)

		orderMenu.displayMainMenu();
	}

}
