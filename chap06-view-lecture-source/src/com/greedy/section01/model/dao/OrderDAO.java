package com.greedy.section01.model.dao;

import static com.greedy.common.JDBCTemplate.close;

import java.awt.Taskbar.State;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;

public class OrderDAO {

	private Properties prop = new Properties();
	
	public OrderDAO() {
		try {
			prop.loadFromXML(new FileInputStream("mapper/order-query.xml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/* 카테고리 전체 조회용 메소드 */
	public List<CategoryDTO> selectAllCategory(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		
		List<CategoryDTO> categoryList = null;
		
		String query = prop.getProperty("selectAllCategory");
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			categoryList = new ArrayList<>();
			while(rset.next()) {
				// 카테고리 번호와 명만 필요하기 때문에 두개만 뽑는다.
				
				CategoryDTO category = new CategoryDTO();
				category.setCode(rset.getInt("CATEGORY_CODE"));
				category.setName(rset.getString("CATEGORY_NAME"));
				
				categoryList.add(category);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(stmt);
		}
		
		// view까지 계속 return된다.
		return categoryList;
	}

	public List<MenuDTO> SelectMenuBy(Connection con, int categoryCode) {
		PreparedStatement pstmt = null;
		// pstmt는 사용자 입력값을 사용할 때 활용
		ResultSet rset = null;
		// rset은 받을 행이 있을 경우 즉,(SELECT문)
		
		List<MenuDTO> menuList = null;
		
		String query = prop.getProperty("selectMenuByCategory");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, categoryCode);
			
			
			menuList = new ArrayList<>();
			rset = pstmt.executeQuery();
			while(rset.next()) {
				MenuDTO menu = new MenuDTO();
				menu.setCode(rset.getInt("MENU_CODE"));
				menu.setName(rset.getString("MENU_NAME"));
				menu.setPrice(rset.getInt("MENU_PRICE"));
				menu.setCategoryCode(rset.getInt("CATEGORY_CODE"));
				menu.setOrderableStatus(rset.getString("ORDERABLE_STATUS"));
				// 반환된 ResultSet의 행의 값에서 해당 컬럼의 값을 DTO자료형에 맞춰 꺼낸다.
				// 만약 get(자료형)이 헷갈린다면 DTO에 set(명칭)과 맞추면 된다.
				
				menuList.add(menu);
				// 꺼내 menuDTO에 담은 값은 List에 한 행 한 행씩 담는다.
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
		

		return menuList;
		// 반드시 호출한 쪽으로 반환
	}

	public int registOrder(Connection con, OrderDTO order) {
		System.out.println("service로 부터 받은 order: " + order);
		// 각 계층별 메소드 이름을 맞추는 이유는 가독성. DAO를 보고 service에서 메소드 확인할 수 있기 대문
		// 가공처리 및 매핑하기
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertOrder");
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, order.getDate());
			pstmt.setString(2, order.getTime());
			pstmt.setInt(3, order.getTotalOrderPrice());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}

	public int registLastOrderCode(Connection con) {
		Statement stmt = null;
		ResultSet rset = null;
		
		int lastOrderCode = 0;
		
		String query = prop.getProperty("selectLastOrderCode");
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				lastOrderCode = rset.getInt(1);
				// getInt(1) : ResultSet의 1번째 컬럼을 int형으로 가져온다.
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(stmt);
		}
		
		return lastOrderCode;
	}

	public int registOrderMenu(Connection con, OrderMenuDTO orderMenu) {
		System.out.println("service로 부터 받은 orderMenu: " + orderMenu);
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query = prop.getProperty("insertOrderMenu");
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, orderMenu.getOrderCode());
			pstmt.setInt(2, orderMenu.getMenuCode());
			pstmt.setInt(3, orderMenu.getAmount());
			
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
}
