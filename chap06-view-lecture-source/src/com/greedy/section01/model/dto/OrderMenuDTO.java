package com.greedy.section01.model.dto;

import java.io.Serializable;

public class OrderMenuDTO implements Serializable{
	private static final long serialVersionUID = 7093287973173621999L;
	
	private int orderCode;		// 주문코드
	private int menuCode;	// 메뉴코드
	private int amount;		// 수량
	
	public OrderMenuDTO() {
	}

	public OrderMenuDTO(int orderCode, int menuCode, int amount) {
		this.orderCode = orderCode;
		this.menuCode = menuCode;
		this.amount = amount;
	}

	public int getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(int orderCode) {
		this.orderCode = orderCode;
	}

	public int getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(int menuCode) {
		this.menuCode = menuCode;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "OrderMenuDTO [orderCode=" + orderCode + ", menuCode=" + menuCode + ", amount=" + amount + "]";
	}
	
	

	
	
	
	
}
 