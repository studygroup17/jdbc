package com.greedy.section01.model.dto;

import java.io.Serializable;
import java.util.List;

public class OrderDTO implements Serializable{
	private static final long serialVersionUID = 8795464947413077599L;

	private int code;				// 주문코드
	private String date;			// 주문날짜
	private String time;			// 주문시간
	private int totalOrderPrice;	// 주문총금액
	
	/* Service로 주문한 건에 대한 내용을 하나로 묶을 때 추가할 내용 */
	private List<OrderMenuDTO> orderMenuList; // 주문 한건에 주문한 메뉴들을 모두 담기위한 속성. OrderDTO 컬럼과 연관관계(참조)가 있기 때문에 추가

	public OrderDTO() {
	}

	public OrderDTO(int code, String date, String time, int totalOrderPrice, List<OrderMenuDTO> orderMenuList) {
		this.code = code;
		this.date = date;
		this.time = time;
		this.totalOrderPrice = totalOrderPrice;
		this.orderMenuList = orderMenuList;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getTotalOrderPrice() {
		return totalOrderPrice;
	}

	public void setTotalOrderPrice(int totalOrderPrice) {
		this.totalOrderPrice = totalOrderPrice;
	}

	public List<OrderMenuDTO> getOrderMenuList() {
		return orderMenuList;
	}

	public void setOrderMenuList(List<OrderMenuDTO> orderMenuList) {
		this.orderMenuList = orderMenuList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "OrderDTO [code=" + code + ", date=" + date + ", time=" + time + ", totalOrderPrice=" + totalOrderPrice
				+ ", orderMenuList=" + orderMenuList + "]";
	}
	
	
	
}
