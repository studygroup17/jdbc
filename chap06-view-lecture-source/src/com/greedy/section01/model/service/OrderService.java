package com.greedy.section01.model.service;

import static com.greedy.common.JDBCTemplate.close;
import static com.greedy.common.JDBCTemplate.getConnection;
import static com.greedy.common.JDBCTemplate.commit;
import static com.greedy.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.List;

import com.greedy.section01.model.dao.OrderDAO;
import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;

public class OrderService {
	
	private OrderDAO orderDAO = new OrderDAO();
	// mvc 구조는 순서가 정확히 순서가 부여되어 있기 때문에 무조건 SERVICE다음 DAO가 오게끔.
	
	/* 카테고리 전체 조회용 메소드 */
	public List<CategoryDTO> SelectAllCategory() {
		
		/* 1. Connection 생성 */
		Connection con = getConnection();
		
		/* 2. DAO의 모든 카테고리 조회용 메소드를 Connection을 넘기며 결과 리턴받기 */
		List<CategoryDTO> categoryList = orderDAO.selectAllCategory(con);
		// List<CategoryDTO>를 사용하는 것은 이미 쿼리의 결과를 알고 있기 때문에 가능.
		// 리턴 받을 카테고리 조회값을 다중행이기 때문에 List사용.
		
		/* 3. Connection 닫기 */
		// connection close는 트랜잭션 단위별로 열고 닫아야 한다.
		// 즉, 반드시 지역변수로 사용
		close(con);
		
		/* 4. 값 반환 */
		// 담은 값은 화면에 보이기 위함 이므로 return
		return categoryList;
	}
	
	/* 사용자로부터 입력받은 값으로 카테고리별 주문 가능한 메뉴 조회용 메소드 */
	// 서비스 계층에 오기전 반드시 입력값을 가공처리해서 받기, 약속
	public List<MenuDTO> SelectMenuBy(int categoryCode) {
		
		/* 1. Connection 생성 */
		Connection con = getConnection();
		
		/* 2. DAO의 해당 카테고리 메뉴를 조회하는 메소드로 categoryCode를 Connection 객체와 같이 전달하여 조회 */
		List<MenuDTO> menuList = orderDAO.SelectMenuBy(con, categoryCode);
		// 서비스계층에서 DAO계층을 생성시 반드시 Connection은 넘겨줘야 한다.
		// categoryCode는 사용자 입력값을 넘겨 줄때 사용.
		
		/* 3. Connnection 닫기 */
		close(con);
		
		/* 4. 값 반환 */
		return menuList;
	}

	/* 주문 정보 등록용 메소드 */
	public int registOrder(OrderDTO order) {
		System.out.println("메뉴에서 받은 order: " + order);
		/* 1. Connection 생성 */
		Connection con = getConnection();
		
		/* 
		 * 2. 트랜잭션(하나의 일 처리 단위)의 성공 여부를 하나의 int 값으로 리턴할 변수 선언
		 * 	  (DML 작업이 2개 이상이라 그 결과를 하나의 변수로 저장하기 위한 용도)
		 * 	  (int 말고 boolean으로 처리할 수도 있다.)
		 */
		int result = 0;
		
		/* 3. DAO의 메소드로 앞에서 전달 받은 값과 Connection을 같이 넘겨서 insert 및 select 작업 실행 */
		/* 3-1. TBL_ORDER TABLE에 INSERT */
		int orderResult = orderDAO.registOrder(con, order);
		System.out.println("Service에서 TBL_Order 테이블에 INSERT 확인: " + orderResult);
		
		/* 3-2. 마지막 발생한 시퀀스 조회 */
		int lastOrderCode = orderDAO.registLastOrderCode(con);
		//select 결과가 int이기 때문에 int자료형 선언.
		
		System.out.println("Service에서 마지막 주문 번호 확인: " + lastOrderCode);
		
		/* 3-3. orderMenuList 안에 있는 각각의 OrderMenuDTO에 orderCode 추가(앞에서 입력받지 못하고 3-2에서야 알게 된 값이므로) */
		List<OrderMenuDTO> orderMenuList = order.getOrderMenuList();
		for(int i = 0; i < orderMenuList.size(); i++) {
			OrderMenuDTO orderMenu = orderMenuList.get(i);
			orderMenu.setOrderCode(lastOrderCode);
			// 못집어 넣은 주문번호를 OrderMenuDTO에 다시 집어 넣는다.
		}
		
		/* 3-4. TBL_ORDER_MENU 테이블에서 List에 담긴 OrderMenuDTO 갯수만큼 insert를 하고 결과를 누적한다. */
		int orderMenuResult = 0;
		for(int i = 0; i < orderMenuList.size(); i++) {
			OrderMenuDTO orderMenu = orderMenuList.get(i);
			orderMenuResult += orderDAO.registOrderMenu(con, orderMenu);
		}
		
		/* 4. 성공 여부 판단 후 트랜잭션 처리 */
		if(orderResult > 0 && orderMenuResult == orderMenuList.size()) {
			// orderMenuList.size()라고 정확이 사용해 갯수만큼 들어갔는지 확인
			commit(con);
			result = 1;
		} else {
			rollback(con);
		}
		
		/* 5. Connection 닫기 */
		close(con);
		
		/* 6. 값 반환 */
		return result;
		// 트랜잭션안 각 작업들이 성공할 경우 1을 호출한 곳으로 반환.
	}

}
